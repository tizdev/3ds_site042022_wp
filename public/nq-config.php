<?php

$env = (!empty($_SERVER['SERVER_NAME']) && strpos($_SERVER['SERVER_NAME'], '.newquest.fr') !== false) ? 'PREPROD' : 'PROD';
define( 'NQ_ENV', $env );

/* PAGES */
define('ID_PAGE_ACCUEIL', 8);
define('ID_PAGE_ACTUS', 10);
define('ID_PAGE_CONTACT', 14);
define('ID_PAGE_LEGAL', 33);
define('ID_PAGE_ABOUT', 12);
define('ID_PAGE_RGPD', 3);


/* TAXONOMIES */
//define('ID_TAX_', 0);


/* NAV */
define('ID_NAV_HEADER', 2);

/* CATEGORIES */
//define('ID_CAT_', 0);

/* TAGS */
//define('ID_TAG_', 0);


/* CF7 */
define('ID_CF7_CONTACT', 6);
define('ID_CF7_USER_REGISTRATION', 35);

/* MENUS */
//define('ID_NAV_HEADER', 0);


/* USERS */
//define('ID_USER_', 0);


/* ACF KEYS */
define('ACF_KEY_CONTACT_GENDER', 'field_5b6852829d4c0');
define('ACF_KEY_CONTACT_FIRSTNAME', 'field_5b69a1a652747');
define('ACF_KEY_CONTACT_LASTNAME', 'field_5b69a14952746');
define('ACF_KEY_CONTACT_PHONE', 'field_5b68528b9d4c1');
define('ACF_KEY_CONTACT_EMAIL', 'field_5b69a27852748');


/* API KEYS */
define('GMAPS_API_KEY', '');
define('CAPTCHA_SITE_KEY', '');
define('CAPTCHA_SECRET_KEY', '');
