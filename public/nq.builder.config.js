module.exports = [
    {
        name: 'main',
        src: require('nqjs-builder/src/SVGOManager'),
        options: {
            inputFolder: './wp-content/themes/nq3d/img/svg-src/',
            outputFolder: './wp-content/themes/nq3d/img/svg-icons/',
            watch: true,
            upload: true
        }
    },
    {
        name: 'main',
        src: require('nqjs-builder/src/SASSManager'),
        options: {
            inputFolder: './wp-content/themes/nq3d/sass/',
            outputFolder: './wp-content/themes/nq3d/',
            watch: true,
            upload: true
        }
    }
];