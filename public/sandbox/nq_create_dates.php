<?php

die('----------- NQ SANDBOX > DISABLED -----------');

include(dirname(__FILE__).'/../wp-load.php');

$tab_years = array('2017', '2018', '2019', '2020', '2021', '2022');

foreach($tab_years as $year){

    wp_insert_term( '1er semestre '.$year, 'tax-dates' );
    wp_insert_term( '2ème semestre '.$year, 'tax-dates' );

    wp_insert_term( '1er trimestre '.$year, 'tax-dates' );
    wp_insert_term( '2ème trimestre '.$year, 'tax-dates' );
    wp_insert_term( '3ème trimestre '.$year, 'tax-dates' );
    wp_insert_term( '4ème trimestre '.$year, 'tax-dates' );

}

die('----------- END -----------');
