<?php

die('-- SANDBOX DISABLED --');

include(dirname(__FILE__).'/../wp-load.php');

require_once(dirname(__FILE__) . '/../wp-admin/includes/media.php');
require_once(dirname(__FILE__) . '/../wp-admin/includes/file.php');
require_once(dirname(__FILE__) . '/../wp-admin/includes/image.php');


$NBR_POST_CREATION = 25;


for($c = 0; $c < $NBR_POST_CREATION; $c++){


    $wptitle = 'ARTICLE NQ FR #'.($c+1);


    echo $wptitle.'<br />';


    $new_wppost_args = array(
        'post_title'    => $wptitle,
        'post_status'   => 'publish',
        'post_type'     => 'post',
        'post_content'  => ''
    );


    $new_wppost_id = wp_insert_post($new_wppost_args);

    if(!empty($new_wppost_id)) {


        $image_url = 'https://img.newquest.fr/1000/600/';

        $upload_dir = wp_upload_dir();
        $image_data = file_get_contents($image_url);
        $filename = 'nq_test_image_' . basename($image_url);

        if (wp_mkdir_p($upload_dir['path'])) {
            $file = $upload_dir['path'] . '/' . $filename;
        } else {
            $file = $upload_dir['basedir'] . '/' . $filename;
        }
        file_put_contents($file, $image_data);

        $wp_filetype = wp_check_filetype($filename, null);
        $attachment = array(
            'post_mime_type' => 'image/png',
            'post_title' => sanitize_file_name($filename),
            'post_content' => '',
            'post_status' => 'inherit'
        );

        $attach_id = wp_insert_attachment($attachment, $file, $new_wppost_id);
        $attach_data = wp_generate_attachment_metadata($attach_id, $file);
        wp_update_attachment_metadata($attach_id, $attach_data);
        set_post_thumbnail($new_wppost_id, $attach_id);


        sleep(1);


    }
}


die('----');