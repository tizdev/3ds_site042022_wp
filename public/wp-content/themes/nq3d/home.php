<?php get_header(); ?>


    <header class="page-header">
        <div class="container">
            <?php
                echo '<div class="breadcrumb">' .nq_inlinesvg('chevron'). '<a href="' .home_url(). '" title="' .__('Accueil', 'nq3d'). '">' .__('Accueil', 'nq3d'). '</a></div>';
                echo '<h1>' .__('Actualités', 'nq3d'). '</h1>';
            ?>
        </div>
    </header>

    <section class="posts-listing">
        <div class="container">

            <?php if ( have_posts() ) {

                global $iteration_posts;
                $iteration_posts = 0;

                while ( have_posts() ) : the_post();
                    get_template_part('loop', 'posts');
                    $iteration_posts++;
                endwhile;

                if ( function_exists('nq_pagination')){
                    nq_pagination();
                }

            } else {

                echo '<p class="noresult">'.__('Aucune actualité pour le moment.','nq3d').'</p>';

            } ?>

        </div>
    </section>

<?php


get_footer();