<?php
get_header();

$page_title = get_the_title();
$page_id = get_the_ID();


echo '<section class="container">';

    echo '<header class="page-header">';
        echo '<div class="breadcrumb">' .nq_inlinesvg('chevron'). '<a href="' .home_url(). '" title="' .__('Accueil', 'nq3d'). '">' .__('Accueil', 'nq3d'). '</a></div>';
        echo '<h1 class="post-title">' .$page_title. '</h1>';
    echo '</header>';

    echo '<div class="page-content global-cms-style">';
        while ( have_posts() ) : the_post();
            echo apply_filters('the_content', get_the_content());
        endwhile;
    echo '</div>';

    if ( $page_id === ID_PAGE_ABOUT ) {
        echo '<div class="page-action">';
            echo '<a class="btn btn-black" href="' .get_permalink(ID_PAGE_ACTUS). '">' .__('Voir la liste d\'articles', 'nq3d'). '</a>';
        echo '</div>';
    }

echo '</section>';

get_footer();