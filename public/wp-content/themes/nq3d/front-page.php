<?php
get_header();

echo '<h1>'.__('3d séléction','nq3d').'</h1>';

// PROGRAMS PREVIEW
// =============================================================================

$args_query_slides = array(
    'post_type' => 'slides',
    'posts_per_page' => 5,
    'post_status' => 'publish'
);

$query_posts_slides = new WP_Query($args_query_slides);

if ($query_posts_slides->have_posts()) :

    $previewCount = 0;

    echo '<section class="h-preview">';
    echo '<div class="container">';

    while ($query_posts_slides->have_posts()) : $previewCount++;

        // Variables
        $query_posts_slides->the_post();
        $preview_id = get_the_ID();
        $preview_about = get_field('about', $preview_id);
        $preview_program_obj = get_field('program', $preview_id);
        $preview_program_id = !empty($preview_program_obj->ID) ? $preview_program_obj->ID : null;

        $preview_localisation = nq_get_tax_infos($preview_program_id,'tax-localisations');

        if (!empty($preview_program_id)) :
            $preview_name = get_the_title($preview_program_id);
            $preview_link = get_permalink($preview_program_id);

            if (has_post_thumbnail($preview_program_id)) {
                $preview_image = wp_get_attachment_image_src(get_post_thumbnail_id($preview_program_id), 'image_720x370');
                $preview_image_url = $preview_image[0];
            } else {
                $preview_image_url = 'https://img.newquest.fr/720/370/5a5a5a';
            }
        endif;


        // Content
        $html_preview = '';

        $html_preview .= '<div class="js-single-slide h-preview-slide' . ($previewCount === 1 ? ' is-visible' : '') . '" data-slide="' . $previewCount . '">';
        $html_preview .= '<div class="h-preview-image"><div class="h-preview-image-content">';
        $html_preview .= !empty($preview_image_url) ? '<img src="' . $preview_image_url . '">' : '';
        $html_preview .= '</div></div>';
        $html_preview .= '<div class="h-preview-content' . (nq_users_can_access() ? ' has-button' : '') . '">';
        $html_preview .= !empty($preview_name) ? '<h2 class="h-preview-title">' . $preview_name . '</h2>' : '';
        $html_preview .= '<span class="h-preview-location">' . $preview_localisation['name'] . '</span>';
        $html_preview .= !empty($preview_about) ? '<p class="h-preview-text">' . $preview_about . '</p>' : '';

        if (nq_users_can_access() && !empty($preview_link)) {
            $html_preview .= '<a class="btn btn-red" href="' . $preview_link . '">' . __('Voir ce programme', 'nq3d') . '</a>';
        }
        $html_preview .= '</div>';
        $html_preview .= '</div>';

        echo $html_preview;

    endwhile;

    if ($previewCount >= 2) :
        echo '<div class="slider-nav-dots">';
        for ($i = 1; $i <= $previewCount; $i++) {
            echo '<div data-index="' . $i . '" class="circle-inner ' . ($i == 1 ? ' is-active' : '') . '"><span class="circle"></span></div>';
        }
        echo '</div>';

        echo '<div class="slider-nav-arrows">';
        echo '<span class="arrow" data-dir="prev">';
        echo nq_inlinesvg('prev');
        echo '</span>';
        echo '<span class="arrow" data-dir="next">';
        echo nq_inlinesvg('next');
        echo '</span>';
        echo '</div>';
    endif;

    echo '</div>';
    echo '</section>';

    wp_reset_query();

endif;


// BANNER
// =============================================================================

if (!nq_users_can_access() && !nq_users_is_pending()) :

    if (have_rows('h-banner')) : ?>

        <section class="h-banner">
            <div class="container">

                <?php while (have_rows('h-banner')) : the_row();

                    // Group variables
                    $banner_text = get_sub_field('text');
                    $banner_image = get_sub_field('image');

                    $banner_image_data = '';

                    !empty($banner_image['alt']) ? $banner_image_data .= 'alt="' . $banner_image['alt'] . '"' : '';
                    !empty($banner_image['title']) ? $banner_image_data .= 'title="' . $banner_image['title'] . '"' : '';

                    if (!empty($banner_image)) {
                        $banner_image_url = $banner_image['sizes']['image_334x250'];


                    }

                    // Group content
                    $html_banner = '';

                    $html_banner .= !empty($banner_image_url) ? '<div class="h-banner-image"><img src="' . $banner_image_url . '"' . $banner_image_data . '></div>' : '';
                    $html_banner .= '<div class="h-banner-content">';
                    $html_banner .= '<div class="h-banner-content-left">';
                    $html_banner .= '<button class="js-btn-connection btn btn-red btn-large" title="' . __('Voir nos programmes', 'nq3d') . '">' . __('Voir nos programmes', 'nq3d') . '</button>';
                    $html_banner .= !empty($banner_text) ? $banner_text : '';
                    $html_banner .= '</div>';
                    $html_banner .= '<div class="h-banner-content-right">';
                    $html_banner .= '<div class="inner">';
                    $html_banner .= '<button class="js-btn-connection btn btn-red" title="' . __('Connectez-vous', 'nq3d') . '">' . __('Connectez-vous', 'nq3d') . '</button>';
                    $html_banner .= '<button class="js-btn-inscription btn btn-black" title="' . __('Demandez l\'accès', 'nq3d') . '">' . __('Demandez l\'accès', 'nq3d') . '</button>';
                    $html_banner .= '<div>' . __('ou ', 'nq3d') . '<a href="#contact">' . __('contactez-nous', 'nq3d') . '</a></div>';
                    $html_banner .= '</div>';
                    $html_banner .= '</div>';
                    $html_banner .= '</div>';

                    echo $html_banner;

                endwhile; ?>

            </div>
        </section>

    <?php endif;

endif;


if (nq_users_is_pending()) :

    echo '<div class="nq-notice-pending">';
    echo '<p>' . __('Votre compte est en attente de validation. Vous allez recevoir un email lors de son activation', 'nq3d') . '</p>';
    echo '</div>';

endif;


// PROGRAMS LIST
// =============================================================================

if (nq_users_can_access()) :

    $args_query_programmes = array(
        'post_type' => 'programmes',
        'posts_per_page' => 10,
        'post_status' => 'publish'
    );

    $query_posts_programmes = new WP_Query($args_query_programmes);

    $programsCount = $query_posts_programmes->post_count;

    if ($query_posts_programmes->have_posts()) : ?>

        <section class="h-programs">
            <div class="container">

                <div class="h-programs-header">
                    <h2><?php _e('Programmes', 'nq3d'); ?></h2>
                    <div class="slider-nav-arrows">
                        <span class="arrow js-programs-slick-prev">
                            <?php echo nq_inlinesvg('prev'); ?>
                        </span>
                        <span class="arrow js-programs-slick-next">
                            <?php echo nq_inlinesvg('next'); ?>
                        </span>
                    </div>
                </div>

                <div class="h-programs-content cards-row<?php echo($programsCount >= 3 ? ' js-programs-slick' : ''); ?>">
                    <?php while ($query_posts_programmes->have_posts()) :
                        $query_posts_programmes->the_post();
                        get_template_part('loop', 'programmes');
                    endwhile; ?>
                </div>

                <a href="<?php echo get_post_type_archive_link('programmes'); ?>"
                   class="btn btn-red"><?php _e('Voir tous les programmes', 'nq3d'); ?></a>

            </div>
        </section>

        <?php wp_reset_query();

    endif;

endif;


// ABOUT
// =============================================================================

if (have_rows('h-about')) :

    $aboutCount = 0;
    ?>

    <section class="h-about">
        <div class="container">

            <?php while (have_rows('h-about')) : the_row();
                $aboutCount++;

                // Repeater variables
                $about_title = get_sub_field('title');
                $about_subtitle = get_sub_field('subtitle');
                $about_text = get_sub_field('text');
                $about_image = get_sub_field('image');

                $about_image_data = '';
                !empty($about_image['alt']) ? $about_image_data .= 'alt="' . $about_image['alt'] . '"' : '';
                !empty($about_image['title']) ? $about_image_data .= 'title="' . $about_image['title'] . '"' : '';

                if (!empty($about_image['sizes']['image_680x390'])) {
                    $about_image_url = $about_image['sizes']['image_680x390'];
                } else {
                    $about_image_url = 'https://img.newquest.fr/680/390/222'; // Falback
                }


                // Repeater content
                $html_about = '';

                $html_about .= '<div class="js-single-slide h-about-slide' . ($aboutCount === 1 ? ' is-visible' : '') . '" data-slide="' . $aboutCount . '">';
                $html_about .= '<div class="h-about-content">';
                $html_about .= !empty($about_title) ? '<h2 class="h-about-title">' . $about_title . '</h2>' : '';
                $html_about .= !empty($about_subtitle) ? '<span class="h-about-subtitle">' . $about_subtitle . '</span>' : '';
                $html_about .= !empty($about_text) ? '<div class="h-about-text">' . $about_text . '</div>' : '';
                $html_about .= '</div>';
                $html_about .= '<div class="h-about-image">';
                $html_about .= !empty($about_image_url) ? '<img src="' . $about_image_url . '"' . $about_image_data . '>' : '';
                $html_about .= '</div>';
                $html_about .= '</div>';

                echo $html_about;

            endwhile;

            if ($aboutCount >= 2) :
                echo '<div class="slider-nav-dots">';
                for ($about_index = 1; $about_index <= $aboutCount; $about_index++) {
                    echo '<div data-index="' . $about_index . '" class="circle-inner ' . ($about_index == 1 ? ' is-active' : '') . '"><span class="circle"></span></div>';
                }
                echo '</div>';

                echo '<div class="slider-nav-arrows">';
                echo '<span class="arrow" data-dir="prev">';
                echo nq_inlinesvg('prev');
                echo '</span>';
                echo '<span class="arrow" data-dir="next">';
                echo nq_inlinesvg('next');
                echo '</span>';
                echo '</div>';

            endif; ?>

        </div>
    </section>

<?php endif;


// CONTACT
// =============================================================================

if (have_rows('h-contact')) : ?>

    <section class="h-contact" id="contact">
        <div class="container">

            <?php while (have_rows('h-contact')) : the_row();

                // Options variables
                $telephone = get_field('telephone', 'options');
                $address = get_field('address', 'options');
                $opening = get_field('opening', 'options');


                // Group variables
                $contact_text = get_sub_field('text');
                $contact_image = get_sub_field('image');

                $contact_image_data = '';
                !empty($contact_image['alt']) ? $contact_image_data .= 'alt="' . $contact_image['alt'] . '"' : '';
                !empty($contact_image['title']) ? $contact_image_data .= 'title="' . $contact_image['title'] . '"' : '';

                if (!empty($contact_image['sizes']['image_690x330'])) {
                    $contact_image_url = $contact_image['sizes']['image_690x330'];
                } else {
                    $contact_image_url = 'https://img.newquest.fr/680/330/222'; // Falback
                }


                // Group content
                $html_contact = '';

                $html_contact .= '<div class="h-contact-location">';

                if (!empty($contact_image)) {
                    $html_contact .= '<div class="h-contact-image">';
                    $html_contact .= !empty($contact_image) ? '<img src="' . $contact_image_url . '" ' . $contact_image_data . '>' : '';
                    $html_contact .= '<span class="h-contact-marker">';
                    $html_contact .= nq_inlinesvg('logo');
                    $html_contact .= '</span>';
                    $html_contact .= '</div>';
                }

                $html_contact .= '<div class="info-box location-box">';
                $html_contact .= nq_inlinesvg('address');
                $html_contact .= '<div class="info-box-content">';
                $html_contact .= '<h3 class="info-box-title">' . __('Nous trouver', 'nq3d') . '</h3>';
                $html_contact .= !empty($address) ? '<div class="info-box-address">' . $address . '</div>' : '';
                $html_contact .= '</div>';
                $html_contact .= '</div>';
                $html_contact .= '<div class="info-box tel-box">';
                $html_contact .= nq_inlinesvg('phone');
                $html_contact .= '<div class="info-box-content">';
                $html_contact .= !empty($telephone) ? '<h3 class="info-box-title">' . $telephone . '</h3>' : '';
                $html_contact .= !empty($opening) ? '<div class="info-box-opening">' . $opening . '</div>' : '';
                $html_contact .= '</div>';
                $html_contact .= '</div>';
                $html_contact .= '</div>';
                $html_contact .= '<div class="h-contact-form">';
                $html_contact .= '<span class="h-contact-form-subtitle">' . __('Une question ?', 'nq3d') . '</span>';
                $html_contact .= '<h2 class="h-contact-form-title">' . __('Contactez-nous', 'nq3d') . '</h2>';
                $html_contact .= !empty($contact_text) ? '<div class="h-contact-form-text">' . $contact_text . '</div>' : '';
                $html_contact .= do_shortcode('[contact-form-7 id="6" title="Formulaire de contact"]');
                $html_contact .= '</div>';

                echo $html_contact;

            endwhile ?>

        </div>
    </section>

<?php endif;


get_footer();
