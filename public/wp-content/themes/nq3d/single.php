<?php
get_header();

$post_id = get_the_ID();
$post_title = get_the_title();
$post_date = get_the_date('d F Y');
$post_thumb_id = get_post_thumbnail_id();




if ( has_post_thumbnail($post_id) ) {
    $post_thumb_url = wp_get_attachment_image_src($post_thumb_id,'image_1120x540', true);
    $post_image_url = $post_thumb_url[0];
}

$imgNone = empty($post_image_url) ? ' imgNone' : '';

echo '<article class="single-post container'.$imgNone.'">';
    echo '<header class="page-header">';
        echo '<div class="breadcrumb">' .nq_inlinesvg('chevron'). '<a href="' .get_permalink(ID_PAGE_ACTUS). '" title="' .__('Articles', 'nq3d'). '">' .__('Articles', 'nq3d'). '</a></div>';
        echo !empty($post_image_url) ? '<img src="' .$post_image_url. '" alt="' .esc_attr($post_title). '"/>': '';
        echo '<h1 class="post-title">' .$post_title. '</h1>';
        echo '<span class="post-date">' .$post_date. '</span>';
    echo '</header>';

    while ( have_posts() ) : the_post();
        echo '<div class="page-content global-cms-style">';
            echo apply_filters('the_content', get_the_content());
        echo '</div>';
    endwhile;

echo '</article>';

echo '<div class="page-action">';
    echo '<a class="btn btn-black" href="' .get_permalink(ID_PAGE_ACTUS). '">' .__('Voir la liste d\'articles', 'nq3d'). '</a>';
echo '</div>';



get_footer();