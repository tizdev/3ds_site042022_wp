<!doctype html>
<html <?php language_attributes(); ?>>
<head>

    <?php
    $color = "#be1a2b";
    echo '<meta name="theme-color" content="'.$color.'">';
    echo '<meta name="msapplication-navbutton-color" content="'.$color.'">';
    ?>

    <?php /*
    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-129189610-1"></script>
    <script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());

        gtag('config', 'UA-129189610-1');
    </script>
    */ ?>

	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="https://gmpg.org/xfn/11">

    <link rel="apple-touch-icon" sizes="180x180" href="<?php echo get_template_directory_uri(); ?>/img/favicon/apple-touch-icon.png">
    <link rel="icon" type="image/png" sizes="32x32" href="<?php echo get_template_directory_uri(); ?>/img/favicon/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="<?php echo get_template_directory_uri(); ?>/img/favicon/favicon-16x16.png">
    <link rel="manifest" href="<?php echo get_template_directory_uri(); ?>/img/favicon/site.webmanifest">
    <link rel="mask-icon" href="<?php echo get_template_directory_uri(); ?>/img/favicon/safari-pinned-tab.svg" color="#be1a2b">
    <meta name="msapplication-TileColor" content="#be1a2b">
    <meta name="theme-color" content="#ffffff">

    <?php wp_head(); ?>

    <script>
        (function (htmlElm) {
            var style = document.createElement('a').style;
            style.cssText = 'pointer-events:auto';
            if (style.pointerEvents !== 'auto') {
                htmlElm.className += ' noPointerEvents';
            }
        }(document.documentElement));
    </script>
</head>

<body <?php body_class(); ?>>

<div class="site-page">

	<header class="site-header">
        <div class="container">

						<div class="header-menu-icon js-open-menu"><?php echo nq_inlinesvg('menu'); ?></div>

            <div class="header-brand">
                <?php echo '<a class="header-brand-logo" href="' .home_url(). '" title="' .get_bloginfo('name'). '">' .nq_inlinesvg('logo'). '</a>'; ?>
                <div class="header-brand-title">
                    <div class="brand-title-up"><span><?php _e('3D','nq3d'); ?></span> <span><?php _e('sélection','nq3d'); ?></span></div>
                    <div class="brand-title-down"><?php _e('Programmes neufs et locations meublées','nq3d'); ?></div>
                </div>
            </div>

            <div class="header-infos">
                <div class="header-infos-contact">
                    <?php
                        $telephone = get_field('telephone', 'options');
                        if ( !empty($telephone) ) {
                            echo nq_inlinesvg('phone');
                            echo '<span>' .__('Contactez-nous','nq3d'). '</span><a href="tel:'.$telephone.'">'.$telephone.'</a>';
                        }
                    ?>
                </div>
                <?php
                    if ( nq_users_can_access() || nq_users_is_pending() ) {
                        $user_data = wp_get_current_user();
                        $user_id = $user_data->ID;
                        $user_name = $user_data->user_lastname;
                        $user_gender = get_field('gender', 'user_' .$user_id);

                        echo '<a class="header-infos-logout" href="' .wp_logout_url(home_url()). '" title="' .__('Déconnexion', 'nq3d'). '">'
                                .nq_inlinesvg('logout', 'nq3d'). __('Déconnexion', 'nq3d').
                            '</a>';
                        echo '<span class="header-infos-user">' .nq_inlinesvg('user').$user_gender. ' ' .$user_name. '</span>';

                    } else {

                        echo '<button class="js-btn-connection btn btn-red" title="' .__('Se connecter pour voir les programmes', 'nq3d'). '">'.nq_inlinesvg('user'). __('Voir nos programmes', 'nq3d'). '</button>';

                    }
                ?>
            </div>

            <?php
                $args_header_nav = array(
                    'menu'          => ID_NAV_HEADER,
                    'menu_class'    => 'site-header-nav-ul',
                    'menu_id'       => '',
                    'container'     => ''
                );

                echo '<nav class="header-nav">';
										echo '<div class="header-close-icon js-close-menu">' .nq_inlinesvg('close'). '</div>';
                    wp_nav_menu($args_header_nav);
										if ( nq_users_can_access() ) {
											echo '<a class="mobile-header-action" href="' .wp_logout_url(home_url()). '" title="' .__('Déconnexion', 'nq3d'). '">'.nq_inlinesvg('logout', 'nq3d'). '<span>' .__('Déconnexion', 'nq3d').'</span></a>';
										} else {
											echo '<div class="mobile-header-action js-btn-connection" title="' .__('Connexion', 'nq3d'). '">'.nq_inlinesvg('user', 'nq3d'). '<span>' .__('Connexion', 'nq3d').'</span></div>';
										}
                echo '</nav>';
            ?>

        </div>
	</header>

    <div class="rwd-marker-1024"></div>
    <div class="rwd-marker-768"></div>
    <div class="rwd-marker-640"></div>

	<main class="site-content">
