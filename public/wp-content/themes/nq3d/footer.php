



            <?php
            if ( !nq_users_can_access() ) {
                if ( function_exists('nq_users_signin_form') ) {
                    nq_users_signin_form();
                }

                if ( function_exists('nq_users_register_form') ) {
                    nq_users_register_form();
                }
            }

            ?>

        </main>

        <footer class="site-footer">
            <div class="footer-center">
                <div class="container">
                    <div class="footer-brand">
                        <?php echo nq_inlinesvg('3dselection'); ?>
                    </div>
                    <div class="footer-contact">
                        <?php
                        $telephone = get_field('telephone', 'options');
                        $address = get_field('address', 'options');

                        echo '<h3>' .__('Nous contacter'). '</h3>';
                        echo ($address ? '<div class="footer-contact-address">' .nq_inlinesvg('address'). '<span>' .$address. '</span></div>' : null);
                        echo ($telephone ? '<div class="footer-contact-tel">' .nq_inlinesvg('phone'). '<span>' .$telephone. '</span></div>' : null);
                        ?>
                    </div>
                </div>
            </div>
            <div class="footer-line">
                <div class="container">
                    <?php echo __('© 3D Sélection 2022', 'nq3d'). ' - <a href="' .get_permalink(ID_PAGE_RGPD). '">'.__('Politique de confidentialité','nq3d').'</a> - <a href="' .get_permalink(ID_PAGE_LEGAL). '">'.__('Mentions légales','nq3d').'</a> - ' .__('Conception :', 'nq3d'). ' <a href="https://www.newquest-group.com/" title="NewQuest group" target="_blank">' .nq_inlinesvg('newquest'). '</a>'; ?>
                </div>
            </div>
        </footer>

    </div>

    <?php wp_footer(); ?>

</body>
</html>
