<?php
// Register Custom Post Type
function nq_register_post_type() {


    // REGISTER POST TYPE >> SLIDER

	$labels_slider = array(
		'name'                  => _x( 'Slider', 'Post Type General Name', 'nq3d' ),
		'singular_name'         => _x( 'Slider', 'Post Type Singular Name', 'nq3d' ),
		'menu_name'             => __( 'Slider', 'nq3d' ),
		'name_admin_bar'        => __( 'Slider', 'nq3d' ),
		'archives'              => __( 'Archives', 'nq3d' ),
		'attributes'            => __( 'Attributs', 'nq3d' ),
		'parent_item_colon'     => __( 'Slide parent:', 'nq3d' ),
		'all_items'             => __( 'Tous les slides', 'nq3d' ),
		'add_new_item'          => __( 'Ajouter un slide', 'nq3d' ),
		'add_new'               => __( 'Ajouter', 'nq3d' ),
		'new_item'              => __( 'Nouveau slide', 'nq3d' ),
		'edit_item'             => __( 'Modifier le slide', 'nq3d' ),
		'update_item'           => __( 'Mettre à jour le slide', 'nq3d' ),
		'view_item'             => __( 'Voir le slide', 'nq3d' ),
		'view_items'            => __( 'Voir les slides', 'nq3d' ),
		'search_items'          => __( 'Rechercher un slide', 'nq3d' ),
		'not_found'             => __( 'Introuvable', 'nq3d' ),
		'not_found_in_trash'    => __( 'Introuvable dans la corbeille', 'nq3d' ),
		'featured_image'        => __( 'Image à la une', 'nq3d' ),
		'set_featured_image'    => __( 'Mettre une image à la une', 'nq3d' ),
		'remove_featured_image' => __( 'Supprimer l\'image à la une', 'nq3d' ),
		'use_featured_image'    => __( 'Utiliser l\'image à la une', 'nq3d' ),
		'insert_into_item'      => __( 'Insérer pour ce slide', 'nq3d' ),
		'uploaded_to_this_item' => __( 'Uploadé sur ce slide', 'nq3d' ),
		'items_list'            => __( 'Liste', 'nq3d' ),
		'items_list_navigation' => __( 'Liste navigation', 'nq3d' ),
		'filter_items_list'     => __( 'Filtrer la liste', 'nq3d' ),
	);
	$args_slider = array(
		'label'                 => __( 'Slider', 'nq3d' ),
		'description'           => __( 'Tous les slides', 'nq3d' ),
		'labels'                => $labels_slider,
		'supports'              => array( 'title', 'thumbnail' ),
		'hierarchical'          => false,
		'public'                => false,
		'show_ui'               => true,
		'show_in_menu'          => true,
		'menu_position'         => 20,
		'menu_icon'             => 'dashicons-slides',
		'show_in_admin_bar'     => true,
		'show_in_nav_menus'     => true,
		'can_export'            => false,
		'has_archive'           => false,
		'exclude_from_search'   => true,
		'publicly_queryable'    => true,
		'capability_type'       => 'page'
	);
	register_post_type( 'slides', $args_slider );




    // REGISTER POST TYPE >> PROGRAMMES

	$labels_programmes = array(
		'name'                  => _x( 'Programmes', 'Post Type General Name', 'nq3d' ),
		'singular_name'         => _x( 'Programme', 'Post Type Singular Name', 'nq3d' ),
		'menu_name'             => __( 'Programmes', 'nq3d' ),
		'name_admin_bar'        => __( 'Programmes', 'nq3d' ),
		'archives'              => __( 'Archives', 'nq3d' ),
		'attributes'            => __( 'Attributs', 'nq3d' ),
		'parent_item_colon'     => __( 'Programme parent:', 'nq3d' ),
		'all_items'             => __( 'Tous les programmes', 'nq3d' ),
		'add_new_item'          => __( 'Ajouter un programme', 'nq3d' ),
		'add_new'               => __( 'Ajouter', 'nq3d' ),
		'new_item'              => __( 'Nouveau programme', 'nq3d' ),
		'edit_item'             => __( 'Modifier le programme', 'nq3d' ),
		'update_item'           => __( 'Mettre à jour le programme', 'nq3d' ),
		'view_item'             => __( 'Voir le programme', 'nq3d' ),
		'view_items'            => __( 'Voir les programmes', 'nq3d' ),
		'search_items'          => __( 'Rechercher un programme', 'nq3d' ),
		'not_found'             => __( 'Introuvable', 'nq3d' ),
		'not_found_in_trash'    => __( 'Introuvable dans la corbeille', 'nq3d' ),
		'featured_image'        => __( 'Image à la une', 'nq3d' ),
		'set_featured_image'    => __( 'Mettre une image à la une', 'nq3d' ),
		'remove_featured_image' => __( 'Supprimer l\'image à la une', 'nq3d' ),
		'use_featured_image'    => __( 'Utiliser l\'image à la une', 'nq3d' ),
		'insert_into_item'      => __( 'Insérer pour ce programme', 'nq3d' ),
		'uploaded_to_this_item' => __( 'Uploadé sur ce programme', 'nq3d' ),
		'items_list'            => __( 'Liste', 'nq3d' ),
		'items_list_navigation' => __( 'Liste navigation', 'nq3d' ),
		'filter_items_list'     => __( 'Filtrer la liste', 'nq3d' ),
	);
	$args_programmes = array(
		'label'                 => __( 'Programmes', 'nq3d' ),
		'description'           => __( 'Tous les programmes', 'nq3d' ),
		'labels'                => $labels_programmes,
		'supports'              => array( 'title', 'thumbnail', 'editor' ),
		'menu_icon'             => 'dashicons-admin-home',
		'hierarchical'          => false,
		'public'                => true,
		'show_ui'               => true,
		'show_in_menu'          => true,
		'menu_position'         => 20,
		'show_in_admin_bar'     => true,
		'show_in_nav_menus'     => true,
		'can_export'            => true,
		'has_archive'           => true,
		'exclude_from_search'   => false,
		'publicly_queryable'    => true,
		'capability_type'       => 'page'
	);

	register_post_type( 'programmes', $args_programmes );



}

add_action( 'init', 'nq_register_post_type', 0 );






// Register Custom Taxonomy
function nq_register_custom_taxonomy() {


	$labels_promoteurs = array(
		'name'                       => _x( 'Promoteurs', 'Taxonomy General Name', 'nq3d' ),
		'singular_name'              => _x( 'Promoteur', 'Taxonomy Singular Name', 'nq3d' ),
		'menu_name'                  => __( 'Promoteurs', 'nq3d' ),
		'all_items'                  => __( 'Tous les promoteurs', 'nq3d' ),
		'parent_item'                => __( 'Promoteur parent', 'nq3d' ),
		'parent_item_colon'          => __( 'Promoteur parent:', 'nq3d' ),
		'new_item_name'              => __( 'Nom du nouveau promoteur', 'nq3d' ),
		'add_new_item'               => __( 'Ajouter un promoteur', 'nq3d' ),
		'edit_item'                  => __( 'Modifier le promoteur', 'nq3d' ),
		'update_item'                => __( 'Mettre à jour le promoteur', 'nq3d' ),
		'view_item'                  => __( 'Voir le promoteur', 'nq3d' ),
		'separate_items_with_commas' => __( 'Séparer les promoteurs avec des virgules', 'nq3d' ),
		'add_or_remove_items'        => __( 'Ajouter/supprimer des promoteurs', 'nq3d' ),
		'choose_from_most_used'      => __( 'Choisir parmis les plus utilisés', 'nq3d' ),
		'popular_items'              => __( 'Promoteurs populaires', 'nq3d' ),
		'search_items'               => __( 'Rechercher un promoteur', 'nq3d' ),
		'not_found'                  => __( 'Introuvable', 'nq3d' ),
		'no_terms'                   => __( 'Pas de promoteur', 'nq3d' ),
		'items_list'                 => __( 'Liste des promoteurs', 'nq3d' ),
		'items_list_navigation'      => __( 'Liste des promoteurs nav', 'nq3d' ),
	);
	$args_promoteurs = array(
		'labels'                     => $labels_promoteurs,
		'hierarchical'               => true,
		'public'                     => true,
		'show_ui'                    => true,
		'show_admin_column'          => true,
		'show_in_nav_menus'          => true,
		'show_tagcloud'              => true,
		'rewrite'					 => false
	);

	register_taxonomy( 'tax-promoteurs', array( 'programmes' ), $args_promoteurs );


	$labels_localisations = array(
		'name'                       => _x( 'Localisations', 'Taxonomy General Name', 'nq3d' ),
		'singular_name'              => _x( 'Localisation', 'Taxonomy Singular Name', 'nq3d' ),
		'menu_name'                  => __( 'Localisations', 'nq3d' ),
		'all_items'                  => __( 'Toutes les localisations', 'nq3d' ),
		'parent_item'                => __( 'Localisation parente', 'nq3d' ),
		'parent_item_colon'          => __( 'Localisation parente:', 'nq3d' ),
		'new_item_name'              => __( 'Nom de la nouvelle localisation', 'nq3d' ),
		'add_new_item'               => __( 'Ajouter une localisation', 'nq3d' ),
		'edit_item'                  => __( 'Modifier la localisation', 'nq3d' ),
		'update_item'                => __( 'Mettre à jour la localisation', 'nq3d' ),
		'view_item'                  => __( 'Voir la localisation', 'nq3d' ),
		'separate_items_with_commas' => __( 'Séparer les localisations avec des virgules', 'nq3d' ),
		'add_or_remove_items'        => __( 'Ajouter/supprimer des localisations', 'nq3d' ),
		'choose_from_most_used'      => __( 'Choisir parmis les plus utilisées', 'nq3d' ),
		'popular_items'              => __( 'Localisations populaires', 'nq3d' ),
		'search_items'               => __( 'Rechercher une localisation', 'nq3d' ),
		'not_found'                  => __( 'Introuvable', 'nq3d' ),
		'no_terms'                   => __( 'Pas de localisation', 'nq3d' ),
		'items_list'                 => __( 'Liste des localisations', 'nq3d' ),
		'items_list_navigation'      => __( 'Liste des localisations nav', 'nq3d' ),
	);
	$args_localisations = array(
		'labels'                     => $labels_localisations,
		'hierarchical'               => true,
		'public'                     => true,
		'show_ui'                    => true,
		'show_admin_column'          => true,
		'show_in_nav_menus'          => true,
		'show_tagcloud'              => true,
		'rewrite'					 => false
	);

	register_taxonomy( 'tax-localisations', array( 'programmes' ), $args_localisations );

	$labels_lois = array(
		'name'                       => _x( 'Lois', 'Taxonomy General Name', 'nq3d' ),
		'singular_name'              => _x( 'Loi', 'Taxonomy Singular Name', 'nq3d' ),
		'menu_name'                  => __( 'Lois', 'nq3d' ),
		'all_items'                  => __( 'Toutes les lois', 'nq3d' ),
		'parent_item'                => __( 'Loi parente', 'nq3d' ),
		'parent_item_colon'          => __( 'Loi parente:', 'nq3d' ),
		'new_item_name'              => __( 'Nom de la nouvelle loi', 'nq3d' ),
		'add_new_item'               => __( 'Ajouter une loi', 'nq3d' ),
		'edit_item'                  => __( 'Modifier la loi', 'nq3d' ),
		'update_item'                => __( 'Mettre à jour la loi', 'nq3d' ),
		'view_item'                  => __( 'Voir la loi', 'nq3d' ),
		'separate_items_with_commas' => __( 'Séparer les lois avec des virgules', 'nq3d' ),
		'add_or_remove_items'        => __( 'Ajouter/supprimer des lois', 'nq3d' ),
		'choose_from_most_used'      => __( 'Choisir parmis les plus utilisées', 'nq3d' ),
		'popular_items'              => __( 'Lois populaires', 'nq3d' ),
		'search_items'               => __( 'Rechercher une loi', 'nq3d' ),
		'not_found'                  => __( 'Introuvable', 'nq3d' ),
		'no_terms'                   => __( 'Pas de loi', 'nq3d' ),
		'items_list'                 => __( 'Liste des lois', 'nq3d' ),
		'items_list_navigation'      => __( 'Liste des lois nav', 'nq3d' ),
	);
	$args_lois = array(
		'labels'                     => $labels_lois,
		'hierarchical'               => true,
		'public'                     => true,
		'show_ui'                    => true,
		'show_admin_column'          => true,
		'show_in_nav_menus'          => true,
		'show_tagcloud'              => true,
		'rewrite'					 => false
	);

	register_taxonomy( 'tax-lois', array( 'programmes' ), $args_lois );



	$labels_dates = array(
		'name'                       => _x( 'Dates d\'actabilité', 'Taxonomy General Name', 'nq3d' ),
		'singular_name'              => _x( 'Date d\'actabilité', 'Taxonomy Singular Name', 'nq3d' ),
		'menu_name'                  => __( 'Dates d\'actabilité', 'nq3d' ),
		'all_items'                  => __( 'Toutes les dates', 'nq3d' ),
		'parent_item'                => __( 'Date parente', 'nq3d' ),
		'parent_item_colon'          => __( 'Date parente:', 'nq3d' ),
		'new_item_name'              => __( 'Nom de la nouvelle date', 'nq3d' ),
		'add_new_item'               => __( 'Ajouter une date d\'actabilité', 'nq3d' ),
		'edit_item'                  => __( 'Modifier la date', 'nq3d' ),
		'update_item'                => __( 'Mettre à jour la date', 'nq3d' ),
		'view_item'                  => __( 'Voir la date d\'actabilité', 'nq3d' ),
		'separate_items_with_commas' => __( 'Séparer les dates avec des virgules', 'nq3d' ),
		'add_or_remove_items'        => __( 'Ajouter/supprimer des dates', 'nq3d' ),
		'choose_from_most_used'      => __( 'Choisir parmis les plus utilisées', 'nq3d' ),
		'popular_items'              => __( 'Dates populaires', 'nq3d' ),
		'search_items'               => __( 'Rechercher une date', 'nq3d' ),
		'not_found'                  => __( 'Introuvable', 'nq3d' ),
		'no_terms'                   => __( 'Pas de date', 'nq3d' ),
		'items_list'                 => __( 'Liste des dates', 'nq3d' ),
		'items_list_navigation'      => __( 'Liste des dates nav', 'nq3d' ),
	);
	$args_dates = array(
		'labels'                     => $labels_dates,
		'hierarchical'               => true,
		'public'                     => true,
		'show_ui'                    => true,
		'show_admin_column'          => true,
		'show_in_nav_menus'          => true,
		'show_tagcloud'              => true,
		'rewrite'					 => false
	);

	register_taxonomy( 'tax-dates', array( 'programmes' ), $args_dates );



    $labels_budget = array(
        'name'                       => _x( 'Budget', 'Taxonomy General Name', 'nq3d' ),
        'singular_name'              => _x( 'Budget', 'Taxonomy Singular Name', 'nq3d' ),
        'menu_name'                  => __( 'Budget', 'nq3d' ),
        'all_items'                  => __( 'Toutes les budgets', 'nq3d' ),
        'parent_item'                => __( 'Budget parent', 'nq3d' ),
        'parent_item_colon'          => __( 'Budget parent:', 'nq3d' ),
        'new_item_name'              => __( 'Nom du nouveau budget', 'nq3d' ),
        'add_new_item'               => __( 'Ajouter un budget', 'nq3d' ),
        'edit_item'                  => __( 'Modifier le budget', 'nq3d' ),
        'update_item'                => __( 'Mettre à jour le budget', 'nq3d' ),
        'view_item'                  => __( 'Voir le budget', 'nq3d' ),
        'separate_items_with_commas' => __( 'Séparer les budgets avec des virgules', 'nq3d' ),
        'add_or_remove_items'        => __( 'Ajouter/supprimer des budgets', 'nq3d' ),
        'choose_from_most_used'      => __( 'Choisir parmis les plus utilisées', 'nq3d' ),
        'popular_items'              => __( 'Budgets populaires', 'nq3d' ),
        'search_items'               => __( 'Rechercher un budget', 'nq3d' ),
        'not_found'                  => __( 'Introuvable', 'nq3d' ),
        'no_terms'                   => __( 'Pas de budget', 'nq3d' ),
        'items_list'                 => __( 'Liste des budgets', 'nq3d' ),
        'items_list_navigation'      => __( 'Liste des budgets nav', 'nq3d' ),
    );
    $args_budget = array(
        'labels'                     => $labels_budget,
        'hierarchical'               => true,
        'public'                     => true,
        'show_ui'                    => true,
        'show_admin_column'          => true,
        'show_in_nav_menus'          => true,
        'show_tagcloud'              => true,
        'rewrite'					 => false
    );

    register_taxonomy( 'tax-budgets', array( 'programmes' ), $args_budget );


}
add_action( 'init', 'nq_register_custom_taxonomy', 0 );