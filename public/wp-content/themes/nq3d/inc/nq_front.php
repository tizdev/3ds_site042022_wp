<?php

// NQ > DISABLE ADMIN BAR

add_filter('show_admin_bar' , '__return_false');


// NQ > DISABLE XMLRPC

add_filter( 'xmlrpc_enabled', '__return_false' );
remove_action( 'wp_head', 'rsd_link' );


// NQ > Disable REST API

//remove_action( 'init',          'rest_api_init' );
//remove_action( 'parse_request', 'rest_api_loaded' );
//remove_action( 'xmlrpc_rsd_apis',            'rest_output_rsd' );
//remove_action( 'wp_head',                    'rest_output_link_wp_head' );
//remove_action( 'template_redirect',          'rest_output_link_header', 11 );
//remove_action( 'auth_cookie_malformed',      'rest_cookie_collect_status' );
//remove_action( 'auth_cookie_expired',        'rest_cookie_collect_status' );
//remove_action( 'auth_cookie_bad_username',   'rest_cookie_collect_status' );
//remove_action( 'auth_cookie_bad_hash',       'rest_cookie_collect_status' );
//remove_action( 'auth_cookie_valid',          'rest_cookie_collect_status' );
//add_filter( 'rest_enabled',       '__return_false' );
//add_filter( 'rest_jsonp_enabled', '__return_false' );


// NQ > REMOVE COMMENTS STYLE IN HEADER

function remove_recent_comment_style() {
    global $wp_widget_factory;
    remove_action(
        'wp_head',
        array( $wp_widget_factory->widgets['WP_Widget_Recent_Comments'], 'recent_comments_style' )
    );
}
add_action( 'widgets_init', 'remove_recent_comment_style' );


// NQ > REMOVE EMOJIS IN HEADER

remove_action('wp_head', 'print_emoji_detection_script', 7);
remove_action('admin_print_scripts', 'print_emoji_detection_script');
remove_action('wp_print_styles', 'print_emoji_styles');
remove_action('admin_print_styles', 'print_emoji_styles');
add_filter( 'emoji_svg_url', '__return_false' );


// NQ > OTHER HEADER/FOOTER CLEANUP

remove_action( 'wp_head', 'rest_output_link_wp_head', 10);
remove_action( 'wp_head', 'dns-prefetch' );
remove_action( 'wp_head', 'wp_resource_hints', 2 );



remove_action('wp_head', 'rsd_link');
remove_action('wp_head', 'rel_canonical');
remove_action('wp_head', 'feed_links', 2);
remove_action('wp_head', 'index_rel_link');
remove_action('wp_head', 'wp_generator');
remove_action('wp_head', 'wlwmanifest_link');
remove_action('wp_head', 'feed_links_extra', 3);
remove_action('wp_head', 'start_post_rel_link', 10, 0);
remove_action('wp_head', 'parent_post_rel_link', 10, 0);
remove_action('wp_head', 'adjacent_posts_rel_link', 10, 0);
remove_action('wp_head', 'adjacent_posts_rel_link_wp_head', 10, 0 );
remove_action('wp_head', 'start_post_rel_link' );
remove_action('wp_head', 'wp_shortlink_wp_head' );

add_filter( 'index_rel_link', '__return_false' );
add_filter( 'parent_post_rel_link', '__return_false' );
add_filter( 'start_post_rel_link', '__return_false' );
add_filter( 'previous_post_rel_link', '__return_false' );
add_filter( 'next_post_rel_link', '__return_false' );
add_filter( 'post_comments_feed_link', '__return_false' );


function my_deregister_scripts(){
    wp_deregister_script( 'wp-embed' );
}
add_action( 'wp_footer', 'my_deregister_scripts' );


// =======================================================================
// NQ > REMOVE JS / CSS VERSION NUMBERS
// =======================================================================

function nq_remove_ver_css_js( $src ) {

	if ( strpos( $src, 'ver=' ) )
		$src = remove_query_arg( 'ver', $src );

	return $src;
}

add_filter( 'style_loader_src', 'nq_remove_ver_css_js', 9999 );
add_filter( 'script_loader_src', 'nq_remove_ver_css_js', 9999 );





// =======================================================================
//	NQ > Pagination
// =======================================================================

function nq_pagination() {

    global $wp_query;
    $wp_query->query_vars['paged'] > 1 ? $current = $wp_query->query_vars['paged'] : $current = 1;

    $big = 999999999;
    $pagination = array(
        'base' => str_replace( $big, '%#%', get_pagenum_link( $big ) ),
        'format' => '?paged=%#%',
        'total' => $wp_query->max_num_pages,
        'current' => $current,
        'prev_text' => '<span>'.nq_inlinesvg('prev').'</span>',
        'next_text' => '<span>'.nq_inlinesvg('next').'</span>',
        'type' => 'plain',
        'before_page_number' => '<span class="number">',
        'after_page_number' => '</span>'
    );

    if($wp_query->post_count < $wp_query->found_posts){
        echo '<div class="nq-pagination-outer">';
        echo '<div class="nq-pagination">';
        echo paginate_links( $pagination );
        echo '</div>';
        echo '</div>';
    }
}





// =======================================================================
//	NQ > CF7 > Load JS & CSS only if necessary
// =======================================================================

function nq_dequeue_cf7_scripts() {


    $load_scripts = false;

    if( is_singular() ) {
        $post = get_post();

        if( has_shortcode($post->post_content, 'contact-form-7') ) {
            $load_scripts = true;
        }

    }

    // TODO > CHECK IF NEEDED

//    if( ! $load_scripts ) {
//        wp_dequeue_script( 'contact-form-7' );
//    }


    wp_dequeue_style( 'contact-form-7' );

}

add_action( 'wp_enqueue_scripts', 'nq_dequeue_cf7_scripts', 99 );



// =======================================================================
// print & die functions
// =======================================================================

if(!function_exists('p')){
    function p($array){
        echo "<pre>".print_r($array, true)."</pre>";
    }
}

if(!function_exists('d')){
    function d($array){
        p($array);
        die('END');
    }
}



// =======================================================================
// Image size
// =======================================================================
if ( function_exists( 'add_image_size' ) ) {

    add_image_size( 'image_1400x500', 1440, 500, true );  // => Video (single program)
    add_image_size( 'image_1120x540', 1120, 540, true );  // => Thumbnail (single post)
    add_image_size( 'image_780x385', 780, 385, true );    // => Gallery (single program)
    add_image_size( 'image_720x370', 720, 370, true );    // => Preview (home)
    add_image_size( 'image_600x300', 600, 300, true );    // => Actu (home)
    add_image_size( 'image_690x330', 690, 330, true );    // => Contact (home)
    add_image_size( 'image_680x390', 680, 390, true );    // => About (home)
    add_image_size( 'image_565x270', 565, 270, true );    // => Post item (actuality)
    add_image_size( 'image_330x270', 330, 270, true );    // => Sponsor image (single program)
    add_image_size( 'image_485x323', 485, 323, true );    // => Program card (home / programs)
    add_image_size( 'image_200xauto', 200, 9999, false ); // => Sponsor logo (single program)
    add_image_size( 'image_334x250', 334, 250, true ); // => Img home

}

// ===================================================================
//	NQ > DISABLE SEARCH
// ===================================================================
function nq_disable_search( $query, $error = true ) {
    if ( $query->is_search && $query->is_main_query() ) {
        unset( $_GET['s'] );
        unset( $_POST['s'] );
        unset( $_REQUEST['s'] );
        unset( $query->query['s'] );
        $query->set( 's', '' );
        $query->is_search = false;
        $query->set_404();
        status_header( 404 );
        nocache_headers();
    }
}
add_action( 'parse_query', 'nq_disable_search' );


function nq_custom_jpeg_quality() {

    return 100;

}
add_filter( 'jpeg_quality', 'nq_custom_jpeg_quality' );







// =======================================================================
// FRONT > INLINE SVG
// =======================================================================

function nq_inlinesvg($name, $class = '') {

    $content = @file_get_contents(get_template_directory().'/img/svg-icons/' . $name.'.svg');

    if($content === FALSE) {

        return '';

    } else {

        $content = preg_replace('/\sid=(["\'])([^"\']+)(["\'])/', ' id=$1' . $name . '-$2$3 ', $content);
        $content = preg_replace('/url\(#([^)]+)\)/', 'url(#' . $name . '-$1)', $content);
        $content = preg_replace('/\sxlink:href=(["\'])#([^"\']+)(["\'])/', ' xlink:href=$1#' . $name . '-$2$3 ', $content);

        if(!empty($class)) {

            return preg_replace('/<svg/', '<svg class="svgicon-' . preg_replace('/%name/', $name, $class) . '"', $content);


        } else {

            return preg_replace('/<svg/', '<svg class="svgicon"', $content);

        }

    }

}




function nq_sanitize_svg_ids($prefix = '', $id, $content) {

    $name = $prefix . $id;
    $content = preg_replace('/\sid=(["\'])([^"\']+)(["\'])/', ' id=$1' . $name . '-$2$3 ', $content);
    $content = preg_replace('/url\(#([^)]+)\)/', 'url(#' . $name . '-$1)', $content);
    $content = preg_replace('/\sxlink:href=(["\'])#([^"\']+)(["\'])/', ' xlink:href=$1#' . $name . '-$2$3 ', $content);

    return $content;

}






// =======================================================================
//	NQ > RESPONSIVE VIDEO
// =======================================================================

function nq_custom_oembed_filter($html, $url, $attr, $post_ID) {

    if(strpos($url,'twitter.com') === false) {

        $return = '<div class="video-outer">' . $html . '</div>';
        return $return;

    }else{

        return $html;

    }

}

add_filter( 'embed_oembed_html', 'nq_custom_oembed_filter', 10, 4 ) ;




function nq_display_preprod_message(){

    if( defined('NQ_ENV') && NQ_ENV == 'PREPROD'){

        $html_message_preprod = '';
        $html_message_preprod .= '<div class="nq3d-header-preprod-message ff_bold">';
        $html_message_preprod .= __('Attention ! Version du site en pré-production !','nq3d');
        $html_message_preprod .= '</div>';

        echo $html_message_preprod;

    }

}

//add_action('nq_hook_after_body','nq_display_preprod_message');



// =======================================================================
//	NQ > ?
// =======================================================================

function nq_css_attributes_filter($var) {

    return is_array($var) ? array_intersect($var, array('current-menu-item')) : '';

}

add_filter('nav_menu_css_class', 'nq_css_attributes_filter', 100, 1);
add_filter('nav_menu_item_id', 'nq_css_attributes_filter', 100, 1);



// =======================================================================
// FRONT > PRE GET POSTS SVG
// =======================================================================

function nq_pre_get_posts($query) {

    // BLOG

    if ( !is_admin() && $query->is_main_query() ) {

		// SEARCH
        if ( is_search()) {

            $query->set('posts_per_page', -1);

        }



		// CPT > PROGRAMMES
        if (is_post_type_archive('programmes')) {

            $query->set('posts_per_page', 12);


			$tab_tax_query = array();


			if( get_query_var('localisation') ){

				$secteur_tax = esc_attr( get_query_var('localisation') );

				if(term_exists($secteur_tax, 'tax-localisations')){

					array_push($tab_tax_query ,
						array(
							'taxonomy'  => 'tax-localisations',
							'field'     => 'slug',
							'terms'     => $secteur_tax
						)
					);

				}

			}

			if( get_query_var('promoteur') ){

				$promoteur_tax = esc_attr( get_query_var('promoteur') );

				if(term_exists($promoteur_tax, 'tax-promoteurs')){

					array_push($tab_tax_query ,
						array(
							'taxonomy'  => 'tax-promoteurs',
							'field'     => 'slug',
							'terms'     => $promoteur_tax
						)
					);

				}

			}

			if( get_query_var('date-actabilite') ){

				$date_tax = esc_attr( get_query_var('date-actabilite') );

				if(term_exists($date_tax, 'tax-dates')){

					array_push($tab_tax_query ,
						array(
							'taxonomy'  => 'tax-dates',
							'field'     => 'slug',
							'terms'     => $date_tax
						)
					);

				}

			}

			if( get_query_var('loi') ){

				$loi_tax = esc_attr( get_query_var('loi') );

				if(term_exists($loi_tax, 'tax-lois')){

					array_push($tab_tax_query ,
						array(
							'taxonomy'  => 'tax-lois',
							'field'     => 'slug',
							'terms'     => $loi_tax
						)
					);

				}

			}

            if( get_query_var('budget') ){

                $budget_tax = esc_attr( get_query_var('budget') );

                if(term_exists($budget_tax, 'tax-budgets')){

                    array_push($tab_tax_query ,
                        array(
                            'taxonomy'  => 'tax-budgets',
                            'field'     => 'slug',
                            'terms'     => $budget_tax
                        )
                    );

                }

            }


            if(!empty($tab_tax_query)){

                $tab_tax_query_final = array('relation' => 'AND');
                $tab_tax_query_final = array_merge($tab_tax_query_final, $tab_tax_query);

                $query->set('tax_query', $tab_tax_query_final);

            }


        }


    }

}


add_action('pre_get_posts','nq_pre_get_posts');



// =======================================================================
//	NQ > NEW QUERY VARS
// =======================================================================

function nq_add_query_vars($aVars) {

    $aVars[] = "promoteur";
    $aVars[] = "date-actabilite";
    $aVars[] = "localisation";
    $aVars[] = "loi";
    $aVars[] = "budget";

    return $aVars;
}

add_filter('query_vars', 'nq_add_query_vars');


function nq_custom_rewrite_tag() {

    add_rewrite_tag('%promoteur%', '([^&]+)');
    add_rewrite_tag('%date-actabilite%', '([^&]+)');
    add_rewrite_tag('%localisation%', '([^&]+)');
    add_rewrite_tag('%loi%', '([^&]+)');
    add_rewrite_tag('%budget%', '([^&]+)');

}
add_action('init', 'nq_custom_rewrite_tag', 10, 0);



function nq_custom_rewrite_rule() {

	$programmes_rewrite_slug = 'programmes';
	$programmes_obj = get_post_type_object('programmes');
	if ( !empty($programmes_obj->rewrite['slug']) ) {
		$programmes_rewrite_slug = $programmes_obj->rewrite['slug'];
	}

	add_rewrite_rule(
        '^' . $programmes_rewrite_slug . '/promoteur/([^/]*)/date-actabilite/([^/]*)/localisation/([^/]*)/loi/([^/]*)/budget/([^/]*)/page/([0-9]{1,})/?',
        'index.php?post_type=programmes&promoteur=$matches[1]&date-actabilite=$matches[2]&localisation=$matches[3]&loi=$matches[4]&budget=$matches[5]&paged=$matches[6]',
        'top'
    );

	add_rewrite_rule(
        '^' . $programmes_rewrite_slug . '/promoteur/([^/]*)/date-actabilite/([^/]*)/localisation/([^/]*)/loi/([^/]*)/budget/([^/]*)?',
        'index.php?post_type=programmes&promoteur=$matches[1]&date-actabilite=$matches[2]&localisation=$matches[3]&loi=$matches[4]&budget=$matches[5]',
        'top'
    );

}
add_action('init', 'nq_custom_rewrite_rule');




// =======================================================================
//	NQ > POSTS > NAVIGATION / SHARING
// =======================================================================
function nq_get_tax_infos($programme_id, $taxonomy = 'tax-localisation'){

    $tab_tax_infos = array();

    if(!empty($programme_id)){

        $prog_taxs = get_the_terms($programme_id, $taxonomy);

        if ( $prog_taxs && !is_wp_error( $prog_taxs ) ) {

            $prog_tax = array_pop($prog_taxs);
            $prog_tax_name = $prog_tax->name;
            $prog_tax_slug = $prog_tax->slug;
            $prog_tax_zipcode = get_field('code_postal', $taxonomy.'_'.$prog_tax->term_id);
            $prog_tax_logo = get_field('logo_promoteur', $taxonomy.'_'.$prog_tax->term_id);
            $tab_tax_infos['name'] = $prog_tax_name;
            $tab_tax_infos['slug'] = $prog_tax_slug;
            $tab_tax_infos['zipcode'] = !empty($prog_tax_zipcode) ? $prog_tax_zipcode : '';
            $tab_tax_infos['logo'] = !empty($prog_tax_logo) ? $prog_tax_logo : '';

        }

        if (class_exists('WPSEO_Primary_Term')) {

            $wpseo_primary_term_front = new WPSEO_Primary_Term($taxonomy, $programme_id);
            $wpseo_primary_term_front = $wpseo_primary_term_front->get_primary_term();
            $yoast_term = get_term($wpseo_primary_term_front);

            if (!is_wp_error($yoast_term)) {
                $prog_tax_name = $yoast_term->name;
                $prog_tax_slug = $yoast_term->slug;
                $prog_tax_zipcode = get_field('code_postal', $taxonomy.'_'.$yoast_term->term_id);
                $prog_tax_logo = get_field('logo_promoteur', $taxonomy.'_'.$yoast_term->term_id);
                $tab_tax_infos['name'] = $prog_tax_name;
                $tab_tax_infos['slug'] = $prog_tax_slug;
                $tab_tax_infos['zipcode'] = !empty($prog_tax_zipcode) ? $prog_tax_zipcode : '';
                $tab_tax_infos['logo'] = !empty($prog_tax_logo) ? $prog_tax_logo : '';
            }

        }

    }

    return $tab_tax_infos;

}
