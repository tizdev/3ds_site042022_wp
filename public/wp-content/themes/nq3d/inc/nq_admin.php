<?php
/* ----------------------------------------------------------------------------------- */
// Custom Login Page Logo
/* ----------------------------------------------------------------------------------- */

// ADMIN > LOGO IMAGE

function nq_custom_login_logo() {

    $html_style = '';

    $html_style .= '<style type="text/css">';
    $html_style .= 'h1 a { background:transparent url('.get_template_directory_uri().'/admin/img/login-logo.png) no-repeat top center !important; height:82px !important; width:320px !important; }';
    $html_style .= 'body{background: #f5f5f5;}';
    $html_style .= '#backtoblog{display:none;}';
    $html_style .= '.login #nav{text-align:right; margin: 10px 0 0 0; font-style: italic; }';
    $html_style .= '.nq3d-header-preprod-message{ background: #ffd14f; padding: 15px 0; font-size: 16px; line-height: 22px; text-align: center; position: fixed; left: 0; bottom: 0; right: 0; z-index: 50;}';
    $html_style .= '.wp-core-ui .button-primary { background: #be1a2b; border-color: #be1a2b #be1a2b #be1a2b; box-shadow: 0 1px 0 #be1a2b; color: #fff; text-decoration: none; text-shadow: 0 -1px 1px #be1a2b, 1px 0 1px #be1a2b, 0 1px 1px #be1a2b, -1px 0 1px #be1a2b; }';
    $html_style .= '.login #login_error, .login .message, .login .success { border-left-color: #be1a2b; }';
    $html_style .= 'input[type=text]:focus, input[type=search]:focus, input[type=radio]:focus, input[type=tel]:focus, input[type=time]:focus, input[type=url]:focus, input[type=week]:focus, input[type=password]:focus, input[type=checkbox]:focus, input[type=color]:focus, input[type=date]:focus, input[type=datetime]:focus, input[type=datetime-local]:focus, input[type=email]:focus, input[type=month]:focus, input[type=number]:focus, select:focus, textarea:focus { border-color: #be1a2b; box-shadow: 0 0 2px rgba(190,26,43,.8); outline: #be1a2b;  } ';
    $html_style .= '.wp-core-ui .button-primary.focus, .wp-core-ui .button-primary.hover, .wp-core-ui .button-primary:focus, .wp-core-ui .button-primary:hover { background: #1f1f1f;
   border-color: #1f1f1f; text-shadow: 0 -1px 1px #1f1f1f, 1px 0 1px #1f1f1f, 0 1px 1px #1f1f1f, -1px 0 1px #1f1f1f; outline: #1f1f1f;  box-shadow: 0 1px 0 #1f1f1f; color: #fff;}';
    $html_style .= '.login #backtoblog a:hover, .login #nav a:hover { color: #be1a2b; }';
    $html_style .= 'input[type=checkbox]:checked:before { color: #be1a2b; }';
    $html_style .= '.wp-core-ui .button-primary.focus, .wp-core-ui .button-primary:focus { box-shadow: 0 1px 0 #1f1f1f, 0 0 2px 1px #1f1f1f;
} }';
    $html_style .= '</style>';

    echo $html_style;

}
add_action('login_head', 'nq_custom_login_logo');


// ADMIN > LOGO LINK

function nq_loginpage_custom_link() {

    return home_url();

}
add_filter('login_headerurl','nq_loginpage_custom_link');


// ADMIN > LOGO TITLE

function nq_change_title_on_logo() {

    return get_bloginfo('name');

}
add_filter('login_headertitle', 'nq_change_title_on_logo');



function nq_login_message(  ) {

    if ( defined('NQ_ENV') && NQ_ENV == 'PREPROD'){

        $html_message_preprod = '';
        $html_message_preprod .= '<div class="nq3d-header-preprod-message ff_bold">';
        $html_message_preprod .= __('Attention ! Version du site en pré-production !','nq3d');
        $html_message_preprod .= '</div>';

        echo $html_message_preprod;

    }

}

//add_action( 'login_footer', 'nq_login_message',9999 );


/* ----------------------------------------------------------------------------------- */
//	NQ > Taxonomy display
/* ----------------------------------------------------------------------------------- */

add_filter( 'wp_terms_checklist_args', 'nq_config_wp_terms_checklist_args', 1, 2 );
function nq_config_wp_terms_checklist_args( $args, $post_id ) {

    $args[ 'checked_ontop' ] = false;

    return $args;

}

/*-----------------------------------------------------------------------------------*/
// Custom Admin Footer Credits
/*-----------------------------------------------------------------------------------*/

function nq_footer_admin() {

    if ( defined('NQ_ENV') && NQ_ENV == 'PREPROD'){

        $html_message_preprod = '';
        $html_message_preprod .= '<div class="nq3d-header-preprod-message ff_bold">';
        $html_message_preprod .= __('Attention ! Version du site en pré-production !','nq3d');
        $html_message_preprod .= '</div>';

        echo $html_message_preprod;

    }else{

        echo '';

    }

}
//add_filter('admin_footer_text', 'nq_footer_admin');



/*-----------------------------------------------------------------------------------*/
// REMOVE WP LOGO
/*-----------------------------------------------------------------------------------*/

function nq_config_remove_wp_logo( $wp_admin_bar ) {
    $wp_admin_bar->remove_node( 'wp-logo' );
}
add_action( 'admin_bar_menu', 'nq_config_remove_wp_logo', 999 );



/*-----------------------------------------------------------------------------------*/
// REMOVE COMMENTS PART
/*-----------------------------------------------------------------------------------*/

function nq_admin_bar_render() {

    global $wp_admin_bar;
	$wp_admin_bar->remove_menu('comments');

}
add_action( 'wp_before_admin_bar_render', 'nq_admin_bar_render' );



function nq_remove_comments_menu() {

    remove_submenu_page( 'wpseo_dashboard', 'wpseo_licenses' );
    remove_menu_page( 'edit-comments.php' );

}
add_action( 'admin_menu', 'nq_remove_comments_menu', 999 );


function nq_remove_wpseo_dashboard_overview() {

  remove_meta_box( 'wpseo-dashboard-overview', 'dashboard', 'side' );

}
add_action('wp_dashboard_setup', 'nq_remove_wpseo_dashboard_overview' );



/* -------------------------------------
FILTER MAILS ON NQ ADDRESS
-------------------------------------- */
function nq_config_filterMail( $params ) {
    if($params['to'] === 'wordpress@newquest.fr' && (strpos(strtolower($params['message']), 'commentaire') !== false || strpos(strtolower($params['message']), 'comment') !== false)){
        $params['to'] = '';
    }

    return $params;
}
add_filter( 'wp_mail', 'nq_config_filterMail', 1, 1 );


/* ----------------------------------------------------------------------------------- */
//    LOGIN / ADMIN FAVICON
/* ----------------------------------------------------------------------------------- */

function add_favicon() {
    $favicon_url = get_template_directory_uri(). '/admin/img/favicon_admin.ico';
    echo '<link rel="shortcut icon" href="' . $favicon_url . '" />';
}

add_action('login_head', 'add_favicon');
add_action('admin_head', 'add_favicon');



/*-----------------------------------------------------------------------------------*/
//    Disable default image link
/*----------------------------------------------------------------------------------*/

update_option('image_default_link_type','file');



/* ----------------------------------------------------------------------------------- */
//	NQ > admin CSS / JS
/* ----------------------------------------------------------------------------------- */

function nq_admin_jscss( $hook ) {

    /* CSS */
    
    wp_enqueue_style( 'nq_admin-css', get_template_directory_uri().'/admin/css/nq_admin.css', array(), null);


    /* JS */
    //wp_enqueue_script( 'nq_admin-js', get_template_directory_uri().'/admin/js/nq_admin.js', array( 'jquery' ), null );



}

add_action( 'admin_enqueue_scripts', 'nq_admin_jscss' );




/* ----------------------------------------------------------------------------------- */
//	NQ > ADMIN > WPSEO PRIORITY
/* ----------------------------------------------------------------------------------- */

function nq_lower_wpseo_priority( $html ) {
    return 'low';
}

add_filter( 'wpseo_metabox_prio', 'nq_lower_wpseo_priority' );



/* ----------------------------------------------------------------------------------- */
//	NQ > ACF Options page
/* ----------------------------------------------------------------------------------- */

if( function_exists('acf_add_options_page') ) {

	acf_add_options_page();

}



/* ----------------------------------------------------------------------------------- */
//	NQ > ADMIN GMAPS
/* ----------------------------------------------------------------------------------- */


function nq_acf_google_map_api( $api ){

	$api['key'] = defined('GMAPS_API_KEY') ? GMAPS_API_KEY : '';

	return $api;

}

//add_filter('acf/fields/google_map/api', 'nq_acf_google_map_api');




/* ----------------------------------------------------------------------------------- */
//	NQ > MEDIA UPLOAD FILENAME SANITIZE
/* ----------------------------------------------------------------------------------- */
function nq_sanitize_file_name_chars($filename) {

    $sanitized_filename = remove_accents($filename); // Convert to ASCII

    // Standard replacements
    $invalid = array(
        ' ' => '-',
        '%20' => '-',
        '_' => '-'
    );
    $sanitized_filename = str_replace(array_keys($invalid), array_values($invalid), $sanitized_filename);

    $sanitized_filename = preg_replace('/[^A-Za-z0-9-\. ]/', '', $sanitized_filename); // Remove all non-alphanumeric except .
    $sanitized_filename = preg_replace('/\.(?=.*\.)/', '', $sanitized_filename); // Remove all but last .
    $sanitized_filename = preg_replace('/-+/', '-', $sanitized_filename); // Replace any more than one - in a row
    $sanitized_filename = str_replace('-.', '.', $sanitized_filename); // Remove last - if at the end
    $sanitized_filename = strtolower($sanitized_filename); // Lowercase

    return $sanitized_filename;
    
}
add_filter('sanitize_file_name', 'nq_sanitize_file_name_chars', 10);