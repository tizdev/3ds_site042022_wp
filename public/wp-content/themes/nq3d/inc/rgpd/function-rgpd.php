<?php

function __rgpdGetCookie(){
    $cookie = NQContext::get('cookie-rgpd');

    $values = $cookie->get();
    if(!empty($values)){
        $values = json_decode(stripslashes($values), true);
    }

    return $values;
}

function NQrgpdCheck($key, $type = 'cookie'){
    if($type === 'cookie'){
        $prefix = RGPD_NAME_CHECKBOX_COOKIE;
    }elseif($type === 'tracker'){
        $prefix = RGPD_NAME_CHECKBOX_TRACKER;
    }else{
        die('Invalid type');
    }

    $values = __rgpdGetCookie();
    return !empty($values[$prefix.$key]);
}

function NQrgpdGetTexte($key){

    $page = NQContext::get(RGPD_KEY_PAGE_CONTEXT);
    if(!empty($page->acfs) && !empty($page->acfs['text-all-rgpd'])){
        foreach($page->acfs['text-all-rgpd'] as $text){
            if(!empty($text['key']) && trim($text['key']) == trim($key)){
                return $text['text'];
            }
        }
    }
    return '';
}