<?php

class NQContext{
    protected static $context = array();

    public static function set($name, $value){
        self::$context[$name] = $value;
    }

    public static function get($name){
        if(isset(self::$context[$name])){
            return self::$context[$name];
        }
        return false;
    }
}