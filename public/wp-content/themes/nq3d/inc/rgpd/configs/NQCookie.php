<?php
class NQCookie
{
    private $name = false;
    private $value = "";
    private $time;
    private $domain = false;
    private $path = '/';
    private $secure = false;

    public function create() {
        $_COOKIE[$this->getName()] = $this->getValue();
        return setcookie($this->getName(), $this->getValue(), $this->getTime(), $this->getPath(), $this->getDomain(), $this->getSecure(), true);
    }

    /**
     * Return a cookie
     * @return mixed
     */
    public function get(){
        return isset($_COOKIE[$this->getName()]) ? $_COOKIE[$this->getName()] : false;
    }

    /**
     * Delete cookie.
     * @return bool
     */
    public function delete(){
        unset($_COOKIE[$this->getName()]);
        return setcookie($this->getName(), '', time() - 3600, $this->getPath(), $this->getDomain(), $this->getSecure(), true);
    }


    /**
     * @param $domain
     */
    public function setDomain($domain) {
        $this->domain = $domain;
    }

    /**
     * @return bool
     */
    public function getDomain() {
        return $this->domain;
    }

    /**
     * @param $id
     */
    public function setName($id) {
        $this->name = $id;
    }

    /**
     * @return bool
     */
    public function getName() {
        return $this->name;
    }

    /**
     * @param $path
     */
    public function setPath($path) {
        $this->path = $path;
    }

    /**
     * @return bool
     */
    public function getPath() {
        return $this->path;
    }

    /**
     * @param $secure
     */
    public function setSecure($secure) {
        $this->secure = $secure;
    }

    /**
     * @return bool
     */
    public function getSecure() {
        return $this->secure;
    }

    /**
     * @param $time
     */
    public function setTime($time) {
        if(empty($time)){
            return;
        }

        // Create a date
        $date = new \DateTime();
        // Modify it (+1hours; +1days; +20years; -2days etc)
        $date->modify($time);
        // Store the date in UNIX timestamp.
        $this->time = $date->getTimestamp();
    }

    /**
     * @return bool|int
     */
    public function getTime() {
        return $this->time;
    }

    /**
     * @param string $value
     */
    public function setValue($value) {
        $this->value = $value;
    }

    /**
     * @return string
     */
    public function getValue() {
        return $this->value;
    }
}