<?php

include(__DIR__.'/NQContext.php');
include(__DIR__.'/NQCookie.php');

define('RGPD_POST_TYPE', 'page');
define('RGPD_POST_TITLE', 'RGPD');
define('RGPD_POST_META_KEY', 'nq_is_page_rgpd');
define('RGPD_POST_META_VALUE', 1);

define('RGPD_JS_DIR', __DIR__.'/assets/js/');
define('RGPD_CSS_DIR', __DIR__.'/assets/css/');
define('RGPD_NAME_SUBMIT', 'save-permission-rgpd');

define('RGPD_NAME_CHECKBOX_TRACKER', 'permission-tracker-');
define('RGPD_NAME_CHECKBOX_COOKIES', 'permission-cookies');
define('RGPD_NAME_CHECKBOX_COOKIE', 'permission-cookie-');
define('RGPD_KEY_ERRORS_FORM', 'error-rpdg');

$server_name = $_SERVER['HTTP_HOST'];
/*
if(substr_count($server_name, '.') == 2){
    $temp = explode('.', $server_name);
    array_shift($temp);
    $domain = '.'.implode('.', $temp);
}else{
    $domain = '.'.$server_name;
}
*/

define('RGPD_COOKIE_DOMAIN', $server_name);
define('RGPD_COOKIE_NAME_PERMISSION', 'perms-rgpd');
define('RGPD_COOKIE_NAME_TEST', 'test-rgpd');



define('RGPD_KEY_PAGE_CONTEXT', 'page-rgpd');
define('RGPD_GET_NAME_FULL_ACCEPT', 'rgpd-accept-all');


if(!defined('RGPD_POST_PAGE_ID')){
    define('RGPD_POST_PAGE_ID', 3);
}

$reload = false;

if($reload){
    header('Location:/');
}



add_action('init', function () {

    if (!ACTIVE_RGPD) {
        return '';
    }


    $cookie = new NQCookie();
    $cookie->setName(RGPD_COOKIE_NAME_PERMISSION);
    $cookie->setTime(RGPD_COOKIE_TIME);
    $cookie->setDomain(RGPD_COOKIE_DOMAIN);

    NQContext::set('cookie-rgpd', $cookie);
});