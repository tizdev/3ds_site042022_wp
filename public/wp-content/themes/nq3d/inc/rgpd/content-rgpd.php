<?php

add_filter('the_content', function($content){

    if(is_page(RGPD_POST_PAGE_ID)){
        $content = getContentPageRGPD();
    }

    return $content;

});


add_filter('body_class', function ($classes) {

    if(is_page(RGPD_POST_PAGE_ID) && ACTIVE_RGPD) {
        $classes[] = 'nq-rgpd-page';
    }

    return $classes;

});


function getContentPageRGPD(){

    $errors = NQContext::get(RGPD_KEY_ERRORS_FORM);
    $cookie = NQContext::get('cookie-rgpd');

    $values = $cookie->get();
    if(!empty($values)){
        $values = json_decode(stripslashes($values), true);
    }

    $rgpd_html = '';
    $page = NQContext::get(RGPD_KEY_PAGE_CONTEXT);

    if(!empty($errors) && is_array($errors) && count($errors) > 0){
        $rgpd_html .= '<hr />';
        $rgpd_html .= '<h3 class="rgpd-error" style="color:red;">'._x('Erreurs :','[RGPD Plugin]', 'nq3d').'</h3>';
        $rgpd_html .= '<ul>';
        foreach ($errors as $error) {
            $rgpd_html .= '<li class="rgpd-error" style="color:red;">'.$error.'</li>';
        }
        $rgpd_html .= '</ul>';
        $rgpd_html .= '<hr />';
    }


    if(!empty($page->acfs['text_intro'])){
        $rgpd_html .= '<div class="rgpd-intro">'.$page->acfs['text_intro'].'</div>';
    }


    if(!empty($page->acfs['blocs'])){
        foreach($page->acfs['blocs'] as $bloc){
            if(!empty($bloc['title'])){
                $rgpd_html .= '<h2 class="rgpd-title">'.$bloc['title'].'</h2>';
            }

            if(!empty($bloc['subtitle'])){
                $rgpd_html .= '<h3 class="rgpd-subtitle">'.$bloc['subtitle'].'</h3>';
            }

            if(!empty($bloc['text'])){
                $rgpd_html .= '<div class="rgpd-text">'.$bloc['text'].'</div>';
            }
        }
    }


    if(!empty($page->acfs['title-cookie'])){
        //$rgpd_html .= '<h2 class="rgpd-title rgpd-title-cookie">'.$page->acfs['title-cookie'].'</h2>';
    }


    //$rgpd_html .= '<form method="post" class="nq-c-Form">';


    // NQ RGPD >> COOKIES
    // =============================================


    /*if(!empty($page->acfs['cookies']) && count($page->acfs['cookies']) > 0){

        $rgpd_html .= '<h4 class="rgpd-subtitle-cookie">'._x('Cookies','[RGPD Plugin]', 'nq3d').'</h4>';

        $rgpd_html .= '<table width="100%" cellpadding="0" cellspacing="0" class="nq-rgpd-table">';

        $rgpd_html .= '<thead>';
        $rgpd_html .= '<tr>';
        $rgpd_html .= '<th width="30%">'._x('Nom du cookie','[RGPD Plugin]', 'nq3d').'</th>';
        $rgpd_html .= '<th width="30%">'._x('Description et fonction','[RGPD Plugin]', 'nq3d').'</th>';
        $rgpd_html .= '<th width="30%">'._x('Durée de conservation','[RGPD Plugin]', 'nq3d').'</th>';
        $rgpd_html .= '<th width="10%"></th>';
        $rgpd_html .= '</tr>';
        $rgpd_html .= '</thead>';

        $rgpd_html .= '<tbody>';
        $rgpd_html .= '<tr>';
        $rgpd_html .= '<td width="30%">'.RGPD_COOKIE_NAME_PERMISSION.'</td>';
        $rgpd_html .= '<td width="30%">'.$page->acfs['text_cookie_permission'].'</td>';
        $rgpd_html .= '<td width="30%">'.RGPD_COOKIE_TIME_FR.'</td>';
        $rgpd_html .= '<td width="10%">'._x('requis','[RGPD Plugin]', 'nq3d').'</td>';
        $rgpd_html .= '</tr>';



        foreach($page->acfs['cookies'] as $cookie){

            $name = RGPD_NAME_CHECKBOX_COOKIE.$cookie['key'];
            $checked = (!empty($_POST[$name]) || !empty($values[$name])) ? ' checked="checked"' : '';

            $rgpd_html .= '<tr>';
            $rgpd_html .= '<td width="30%">'.$cookie['name'].'</td>';
            $rgpd_html .= '<td width="30%">'.$cookie['descr'].'</td>';
            $rgpd_html .= '<td width="30%">'.$cookie['time'].'</td>';
            $rgpd_html .= '<td width="10%">';

            if(empty($cookie['require'])) {

                $rgpd_html .= '<div class="nq-rgpd-switch">';
                $rgpd_html .= '<input type="checkbox" name="' . $name . '" id="' . $name . '" class="nq-u-visuallyhidden" value="1"' . $checked . ' />';
                $rgpd_html .= '<label for="' . $name . '" class="">';
                $rgpd_html .= '<span class="nq-u-visuallyhidden">'._x('Autoriser','[RGPD Plugin]', 'nq3d').'</span>';
                $rgpd_html .= '</label>';
                $rgpd_html .= '</div>';

            }else{

                $rgpd_html .= _x('requis','[RGPD Plugin]', 'nq3d');

            }

            $rgpd_html .= '</td>';

            $rgpd_html .= '</tr>';

        }

        $rgpd_html .= '</tbody>';

        $rgpd_html .= '</table>';

    }*/

    // NQ RGPD >> TRACKERS
    // =============================================


    /*if(!empty($page->acfs['trackers']) && count($page->acfs['trackers']) > 0){


        $rgpd_html .= '<h4 class="rgpd-subtitle-cookie">'._x('Trackers','[RGPD Plugin]', 'nq3d').'</h4>';

        $rgpd_html .= '<table width="100%" cellpadding="0" cellspacing="0" class="nq-rgpd-table">';

        $rgpd_html .= '<thead>';

        $rgpd_html .= '<tr>';
        $rgpd_html .= '<th width="45%">'._x('Nom du traceur','[RGPD Plugin]', 'nq3d').'</th>';
        $rgpd_html .= '<th width="45%">'._x('Description et fonction','[RGPD Plugin]', 'nq3d').'</th>';
        $rgpd_html .= '<th width="10%"></th>';
        $rgpd_html .= '</tr>';

        $rgpd_html .= '</thead>';

        $rgpd_html .= '<tbody>';

        foreach($page->acfs['trackers'] as $tracker){

            $name = RGPD_NAME_CHECKBOX_TRACKER.$tracker['key'];
            $checked = '';
            if(!empty($_POST[$name]) || !empty($values[$name])){
                $checked = ' checked="checked"';
            }

            $rgpd_html .= '<tr>';

            $rgpd_html .= '<td width="45%">'.$tracker['name'].'</td>';
            $rgpd_html .= '<td width="45%">'.$tracker['descr'].'</td>';
            $rgpd_html .= '<td width="10%">';

            $rgpd_html .= '<div class="nq-rgpd-switch">';
            $rgpd_html .= '<input type="checkbox" name="'.$name.'" id="'.$name.'" class="nq-u-visuallyhidden" value="1"'.$checked.' />';
            $rgpd_html .= '<label for="'.$name.'">';
            $rgpd_html .= '<span class="nq-u-visuallyhidden">'._x('Autoriser','[RGPD Plugin]', 'nq3d').'</span>';
            $rgpd_html .= '</label>';
            $rgpd_html .= '</div>';

            $rgpd_html .= '</td>';

            $rgpd_html .= '</tr>';

        }

        $rgpd_html .= '</tbody>';

        $rgpd_html .= '</table>';

    }*/

    /*if(function_exists('rgpdButtonForm')){

        rgpdButtonForm($rgpd_html);

    }else{

        $rgpd_html .= '<button name="'.RGPD_NAME_SUBMIT.'" class="nq-rgpd-submit btn btn-red" value="1">'._x('Sauvegarder','[RGPD Plugin]', 'nq3d').'</button>';

    }*/

    //$rgpd_html .= '</form>';


    return $rgpd_html;

}