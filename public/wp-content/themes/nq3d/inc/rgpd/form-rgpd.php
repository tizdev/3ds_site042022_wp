<?php

add_action( 'init', function () {
    $save_cookie = false;
    $reload = false;
    if(!empty($_GET[RGPD_GET_NAME_FULL_ACCEPT]) || !empty($_POST[RGPD_NAME_SUBMIT])){
        $page = NQContext::get(RGPD_KEY_PAGE_CONTEXT);
        $values = array();
    }

    if(!empty($_GET[RGPD_GET_NAME_FULL_ACCEPT])){
        if(is_array($page->acfs['trackers'])){
            foreach($page->acfs['trackers'] as $key => $tracker){
                $values[RGPD_NAME_CHECKBOX_TRACKER.$tracker['key']] = 1;
            }
        }

        if(is_array($page->acfs['cookies'])){
            foreach($page->acfs['cookies'] as $key => $cookie){
                $values[RGPD_NAME_CHECKBOX_COOKIE.$cookie['key']] = 1;
            }
        }


        //$values[RGPD_NAME_CHECKBOX_COOKIES] = 1;
        $save_cookie = true;
        $reload = true;
    }

    if(!empty($_POST[RGPD_NAME_SUBMIT])){

        $errors = array();
        /*if(empty($_POST[RGPD_NAME_CHECKBOX_COOKIES])){
            $errors[] = $page->acfs['error-rgpd1'];
        }else{
            $values[RGPD_NAME_CHECKBOX_COOKIES] = $_POST[RGPD_NAME_CHECKBOX_COOKIES];
        }*/

        if(!empty($page->acfs['trackers'])){
            foreach($page->acfs['trackers'] as $key => $tracker){
                if(empty($_POST[RGPD_NAME_CHECKBOX_TRACKER.$tracker['key']])){
                    $values[RGPD_NAME_CHECKBOX_TRACKER.$tracker['key']] = 0;
                }else{
                    $values[RGPD_NAME_CHECKBOX_TRACKER.$tracker['key']] = 1;
                }
            }
        }

        if(!empty($page->acfs['cookies'])){
            foreach($page->acfs['cookies'] as $key => $cookie){
                if(empty($cookie['require'])){
                    if(empty($_POST[RGPD_NAME_CHECKBOX_COOKIE.$cookie['key']])){
                        $values[RGPD_NAME_CHECKBOX_COOKIE.$cookie['key']] = 0;
                    }else{
                        $values[RGPD_NAME_CHECKBOX_COOKIE.$cookie['key']] = 1;
                    }
                }
            }
        }

        if(count($errors) == 0){
            $save_cookie = true;
        }else{
            NQContext::set(RGPD_KEY_ERRORS_FORM, $errors);
        }

    }

    if($save_cookie){


        $cookie = NQContext::get('cookie-rgpd');
        $cookie->setValue(json_encode($values));
        $cookie->create();
    }

    if($reload){
        if(!empty($_SERVER['HTTP_REFERER'])){
            header('Location:'.$_SERVER['HTTP_REFERER']);
            die();
        }
        header('Location:/');
        die();
    }
});



