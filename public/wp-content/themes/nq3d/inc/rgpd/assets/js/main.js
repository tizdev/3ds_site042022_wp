((function($){
    $(".nq-rgpd-checkbox").switchButton({
        width: 50,
        height: 22,
        button_width: 22,
        show_labels: false,
        on_callback: function(){
            $(this.button_bg).css('backgroundColor', '#4cd49a');
        },
        off_callback: function(){
            $(this.button_bg).css('backgroundColor', '#e04f4f');
        }
    });
})(jQuery));