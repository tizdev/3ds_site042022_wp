<?php

add_action('init', function (){

    if(!ACTIVE_RGPD){
        return '';
    }


    $cookie = NQContext::get('cookie-rgpd');

    $values = $cookie->get();
    $page = NQContext::get(RGPD_KEY_PAGE_CONTEXT);
    $values = json_decode(stripslashes($values), true);

    add_action('wp_footer', function () use($values, $page){
        if(!empty($page->acfs['trackers']) && is_array($page->acfs['trackers'])){
            foreach($page->acfs['trackers'] as $tracker){
                if(!empty($tracker['tag-footer'])){
                    echoTagRGDP($tracker, $values);
                }

            }
        }
    });

    add_action('wp_head', function () use($values, $page){
        if(!empty($page->acfs['trackers']) && is_array($page->acfs['trackers'])){
            foreach($page->acfs['trackers'] as $tracker){
                if(empty($tracker['tag-footer'])){
                    echoTagRGDP($tracker, $values);
                }

            }
        }
    });
});

function echoTagRGDP($tracker, $values){
    $key = $tracker['key'];
    $tag_ok = $tracker['tag'];
    $tag_ko = $tracker['tag_no_valid'];

    if(!empty($values[RGPD_NAME_CHECKBOX_TRACKER.$key])){
        echo $tag_ok;
    }else{
        echo $tag_ko;
    }
}
