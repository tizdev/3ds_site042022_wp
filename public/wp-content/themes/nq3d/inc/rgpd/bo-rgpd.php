<?php



$active = 1;

$idPage = apply_filters('wpml_object_id', RGPD_POST_PAGE_ID, 'page', true);
$ips_brute = get_field('ip-rgpd', $idPage);

if(!empty($ips_brute)){
    $active = 0;
    $ips = explode(',', $ips_brute);

    $my_ip = $_SERVER['REMOTE_ADDR'];
    foreach($ips as $ip){
        if(trim($ip) === trim($my_ip)){
            $active = 1;
        }
    }
}
define('ACTIVE_RGPD', $active);



define('RGPD_COOKIE_TIME', get_field('time-cookie-code', RGPD_POST_PAGE_ID));
define('RGPD_COOKIE_TIME_FR', get_field('time-cookie-user', RGPD_POST_PAGE_ID));

add_action('init', function(){
    $page = get_post(RGPD_POST_PAGE_ID);

    $page->acfs = get_fields(RGPD_POST_PAGE_ID);
    NQContext::set(RGPD_KEY_PAGE_CONTEXT, $page);
});

