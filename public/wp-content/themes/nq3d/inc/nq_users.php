<?php

// ===================================================================
//      NQ Membership > USER CAN ACCESS
// ===================================================================

function nq_users_can_access(){

    $user_can_access = false;

    if(is_user_logged_in()){
        $user_data = wp_get_current_user();
        if(!empty($user_data->roles) && is_array( $user_data->roles ) && ( in_array( 'user_access',  $user_data->roles ) || in_array( 'administrator',  $user_data->roles ) ) ){
            $user_can_access = true;
        }
    }

    return $user_can_access;

}


// ===================================================================
//      NQ Membership > USER IS PENDING
// ===================================================================

function nq_users_is_pending(){

    $user_is_pending = false;

    if(is_user_logged_in()){
        $user_data = wp_get_current_user();
        if(!empty($user_data->roles) && is_array( $user_data->roles ) &&  in_array( 'pending_user_access',  $user_data->roles ) ){
            $user_is_pending = true;
        }
    }

    return $user_is_pending;

}



// ===================================================================
//      NQ Membership > ADMIN > NON ADMIN REDIRECT
// ===================================================================

function nq_users_redirect_non_authorized(){

    if (defined('DOING_AJAX') && DOING_AJAX) {
        return;
    }


    $user_data = wp_get_current_user();

    if(
        !empty($user_data->roles) &&
        is_array( $user_data->roles ) &&
        ( in_array( 'user_access', $user_data->roles ) || in_array( 'pending_user_access', $user_data->roles )  )
    ){

        wp_redirect( home_url() );
        exit();

    }

}

add_action( 'current_screen', 'nq_users_redirect_non_authorized' );



// ===================================================================
//	NQ > ADAPT NAV ITEMS IF LOGGED IN
// ===================================================================

function nq_users_adaptive_menu_items($sorted_menu_items, $args){

    if(!nq_users_can_access()) {

        if (!empty($args->menu) && $args->menu == ID_NAV_HEADER) {

            foreach ($sorted_menu_items as $key_menu_item => $menu_item) {

                if (!empty($menu_item->url) && $menu_item->url == get_post_type_archive_link('programmes')) {

                    unset($sorted_menu_items[$key_menu_item]);

                }

            }

        }
    }

    return $sorted_menu_items;

}

add_filter( 'wp_nav_menu_objects', 'nq_users_adaptive_menu_items', 10, 2 );


// ===================================================================
//    NQ USERS > FRONT > HTML CONNEXION
// ===================================================================

function nq_users_auth_if_login(){

    if (isset($_POST['login_form_submit'])) {
        nq_user_auth($_POST['login_form_user'], $_POST['login_form_password']);
    }
}

add_action( 'init', 'nq_users_auth_if_login');




function nq_users_signin_form() {

    global $nq_login_message;

    $html_sign_form = '';

    $html_sign_form .= '<div class="modal-bg"></div>';
    $html_sign_form .= '<div class="modal connection ' . (!empty($nq_login_message) ? ' connection-onload' : ''). '">';
    $html_sign_form .= '<div class="modal-title">';
    $html_sign_form .= nq_inlinesvg('user');
    $html_sign_form .= '<h2>' .__('Connexion', 'nq3d'). '</h2>';
    $html_sign_form .= '</div>';
    $html_sign_form .= !empty($nq_login_message) ? $nq_login_message : '';
    $html_sign_form .= '<form action="'.$_SERVER['REQUEST_URI'].'" method="POST" class="connection-form">';
    $html_sign_form .= '<input type="text" value="" placeholder="'.__('Identifiant','nq3d').'" name="login_form_user" />';
    $html_sign_form .= '<input type="password" value="" placeholder="'.__('Mot de passe','nq3d').'" name="login_form_password" />';
    $html_sign_form .= '<a href="'.wp_lostpassword_url().'" title="'.__('Mot de passe oublié','nq3d').'">'.__('Mot de passe oublié','nq3d').'</a>';
    $html_sign_form .= '<button type="submit" class="btn btn-red login_form_submit" name="login_form_submit">' .__('Je me connecte','nq3d'). '</span></button>';
    $html_sign_form .= '</form>';
    $html_sign_form .= '<div class="modal-close">';
    $html_sign_form .= nq_inlinesvg("close");
    $html_sign_form .= '</div>';
    $html_sign_form .= '</div>';

    echo $html_sign_form;
}


function nq_user_auth( $username, $password ) {

    global $user, $nq_login_message;

    $creds = array();
    $creds['user_login'] = $username;
    $creds['user_password'] =  $password;
    $creds['remember'] = true;

    $user = wp_signon( $creds, false );

    if ( !is_wp_error($user) ) {

        wp_redirect(get_post_type_archive_link('programmes'));
        exit;

    }else{

        $nq_login_message = '<div class="error" style="margin-top:10px">'.__('Identifiant et/ou mot de passe invalide !', 'nq3d').'</div>';

    }

}


// ===================================================================
//    NQ USERS > FRONT > HTML REGISTER
// ===================================================================

function nq_users_register_form() {

    $html_register_form = '';

    $html_register_form .= '<div class="modal inscription">';
    $html_register_form .= '<div class="modal-title">';
    $html_register_form .= nq_inlinesvg('user');
    $html_register_form .= '<h2>' . __('Nous rejoindre', 'nq3d') . '</h2>';
    $html_register_form .= '</div>';
    $html_register_form .= do_shortcode('[contact-form-7 id="35" title="Formulaire demande d\'inscription"]');
    $html_register_form .= '<div class="modal-close">';
    $html_register_form .= nq_inlinesvg("close");
    $html_register_form .= '</div>';
    $html_register_form .= '</div>';

    echo $html_register_form;
}



// ===================================================================
//    NQ USERS > CF7 > CREATE UNVALIDATED WP USERS
// ===================================================================




/*-----------------------------------------------------------------------------------*/
// NQ > FRONT > STORE CF7 NEWSLETTER OPTIN
/*-----------------------------------------------------------------------------------*/

function nq_cf7_mail_sent ($cf7) {

    if(class_exists('WPCF7_Submission')){

        $submission = WPCF7_Submission::get_instance();

        if ($submission) {

            // On récupère les données du formulaire

            $posted_data = $submission->get_posted_data();

            if(!empty($posted_data)){


                // PRIVATE SPACE ACCESS

                $current_cf7_id = sanitize_text_field($posted_data['_wpcf7']);

                if(!empty($current_cf7_id) && $current_cf7_id == ID_CF7_USER_REGISTRATION){


                    // menu-gender
                    // text-lastname
                    // text-firstname
                    // text-email
                    // text-phone
                    // text-password

                    $nom = sanitize_text_field($posted_data['text-lastname']);
                    $gender = sanitize_text_field($posted_data['menu-gender']);
                    $prenom = sanitize_text_field($posted_data['text-firstname']);
                    $phone = sanitize_text_field($posted_data['text-phone']);
                    $password = sanitize_text_field($posted_data['text-password']);
                    $email = sanitize_email($posted_data['text-email']);

                    if(
                       !empty($nom) &&
                       !empty($prenom) &&
                       !empty($password) &&
                       (!empty($email) && is_email($email) )
                    ) {

                        if ( !username_exists($email) && !email_exists($email)  ) {

                            $lower_email = strtolower($email);
                            $new_user_id = wp_create_user( $lower_email, $password, $lower_email);

                            if( !is_wp_error( $new_user_id ) ) {

                                $new_user = new WP_User($new_user_id);

                                // USER ROLE =================

                                $new_user->set_role('pending_user_access');

                                // USER INFO =================

                                wp_update_user(
                                    array(
                                        'ID'            => $new_user_id,
                                        'first_name'    => $prenom,
                                        'last_name'     => $nom,
                                        'user_email'    => $email
                                    )
                                );


                                $gender_value = '';
                                if(!empty($gender) && $gender == 'M.'){
                                    $gender_value = 1;
                                }elseif(!empty($gender) && $gender == 'Mme.'){
                                    $gender_value = 2;
                                }

                                update_field( ACF_KEY_CONTACT_GENDER, $gender_value , 'user_'.$new_user_id);
                                update_field( ACF_KEY_CONTACT_PHONE, $phone , 'user_'.$new_user_id);
                                update_field( ACF_KEY_CONTACT_FIRSTNAME, $prenom , 'user_'.$new_user_id);
                                update_field( ACF_KEY_CONTACT_LASTNAME, $nom , 'user_'.$new_user_id);
                                update_field( ACF_KEY_CONTACT_EMAIL, $email , 'user_'.$new_user_id);




                            }

                        }

                    }

                }

            }

        }
    }

}

add_action('wpcf7_mail_sent', 'nq_cf7_mail_sent');



/* ----------------------------------------------------------------------------------- */
//  NQ > NOTIFICATION EMAILS
/* ----------------------------------------------------------------------------------- */

function nq_set_email_html_content_type(){

    return 'text/html';

}


// EMAILS > USER ROLE CHANGE ( pending_user_access ==> user_access )

function nq_user_role_update( $user_id, $new_role ) {

    if ($new_role == 'user_access') {

        $user_info = get_userdata( $user_id );

        $mail_to = $user_info->user_email;

        $user_mail = $user_info->user_email;
        $user_login = $user_info->user_login;
        $user_nom = $user_info->user_firstname;
        $user_prenom = $user_info->user_lastname;
        $sitename = '3dselection.com';


        if(!empty($mail_to)){

            $mail_subject = "3D Sélection | Votre compte est désormais activé";
            $mail_headers = 'From: 3D Sélection <ne-pas-repondre@3dselection.com>' . "\r\n";
            $mail_message = '';

            $mail_message .= '<p>Bonjour '.$user_prenom.' '.$user_nom.',</p>';

            $mail_message .= '<p>Nous avons le plaisir de vous informer que votre demande d\'inscription sur le site '.$sitename.' a bien été validée.</p>';

            $mail_message .= '<p>';
            $mail_message .= 'Vous pouvez désormais vous connecter à l\'aide de vos identifiants: <br />';
            $mail_message .= 'Votre nom d\'utilisateur: '.$user_login;
            $mail_message .= '</p>';

            $mail_message .= '<p>Nous vous souhaitons une bonne navigation sur notre site.</p>';

            $mail_message .= '<p>L\'équipe 3D Sélection</p>';

            add_filter('wp_mail_content_type','nq_set_email_html_content_type');
            wp_mail($mail_to, $mail_subject, $mail_message, $mail_headers);
            remove_filter ('wp_mail_content_type','nq_set_email_html_content_type');

        }

    }



}

add_action( 'set_user_role', 'nq_user_role_update', 10, 2);




// ===================================================================
//    NQ USERS > CF7 > ADD PASSWORD FIELD
// ===================================================================

/*

function nq_wpcf7_add_form_tag_k_password() {

	wpcf7_add_form_tag( array('password','password*'), 'nq_wpcf7_k_password_form_tag_handler',array( 'name-attr' => true ) );

}


function nq_wpcf7_k_password_form_tag_handler( $tag ) {
	if ( empty( $tag->name ) ) {
		return '';
	}

	$validation_error = wpcf7_get_validation_error( $tag->name );

	$class = wpcf7_form_controls_class( $tag->type, 'wpcf7-text' );

	if ( $validation_error ) {
		$class .= ' wpcf7-not-valid';
	}

	$atts = array();

	$atts['size'] = $tag->get_size_option( '40' );
	$atts['maxlength'] = $tag->get_maxlength_option();
	$atts['minlength'] = $tag->get_minlength_option();

	if ( $atts['maxlength'] && $atts['minlength'] && $atts['maxlength'] < $atts['minlength'] ) {
		unset( $atts['maxlength'], $atts['minlength'] );
	}

	$atts['class'] = $tag->get_class_option( $class );
	$atts['id'] = $tag->get_id_option();
	$atts['tabindex'] = $tag->get_option( 'tabindex', 'signed_int', true );

	$atts['autocomplete'] = $tag->get_option( 'autocomplete', '[-0-9a-zA-Z]+', true );

	if ( $tag->is_required() ) {
		$atts['aria-required'] = 'true';
	}

	$atts['aria-invalid'] = $validation_error ? 'true' : 'false';

	$value = (string) reset( $tag->values );

	$value = $tag->get_default_option( $value );

	$value = wpcf7_get_hangover( $tag->name, $value );

	$atts['value'] = $value;

	if ( wpcf7_support_html5() ) {
		$atts['type'] = $tag->basetype;
	} else {
		$atts['type'] = 'password';
	}

	$atts['name'] = $tag->name;

	$atts = wpcf7_format_atts( $atts );

	$html = sprintf(
		'<span class="wpcf7-form-control-wrap %1$s"><input %2$s />%3$s</span>',
		sanitize_html_class( $tag->name ), $atts, $validation_error );

	return $html;
}


function nq_wpcf7_k_password_validation_filter( $result, $tag ) {
	$name = $tag->name;

	$value = isset( $_POST[$name] )
		? trim( wp_unslash( strtr( (string) $_POST[$name], "\n", " " ) ) )
		: '';

	if ( 'password' == $tag->basetype ) {
		if ( $tag->is_required() && '' == $value ) {
			$result->invalidate( $tag, wpcf7_get_message( 'invalid_required' ) );
		}
	}

	return $result;
}


add_action( 'wpcf7_init', 'nq_wpcf7_add_form_tag_k_password' );
add_filter( 'wpcf7_validate_password', 'nq_wpcf7_k_password_validation_filter', 10, 2 );
add_filter( 'wpcf7_validate_password*', 'nq_wpcf7_k_password_validation_filter', 10, 2 );

*/
