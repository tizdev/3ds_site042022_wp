<?php
/**
 * NQ 3D Sélection functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package NQ_3D_Sélection
 */

if ( ! function_exists( 'nq3d_setup' ) ) :

	function nq3d_setup() {

		load_theme_textdomain( 'nq3d', get_template_directory() . '/languages' );

		add_theme_support( 'automatic-feed-links' );

		add_theme_support( 'title-tag' );

		add_theme_support( 'post-thumbnails' );

		register_nav_menus( array('menu-1' => esc_html__( 'Primary', 'nq3d' ),) );

		add_theme_support( 'html5', array('search-form', 'comment-form', 'comment-list', 'gallery', 'caption',) );

		add_theme_support( 'customize-selective-refresh-widgets' );

	}
endif;
add_action( 'after_setup_theme', 'nq3d_setup' );



function nq3d_content_width() {
	$GLOBALS['content_width'] = apply_filters( 'nq3d_content_width', 640 );
}
add_action( 'after_setup_theme', 'nq3d_content_width', 0 );



function nq3d_widgets_init() {
	register_sidebar( array(
		'name'          => esc_html__( 'Sidebar', 'nq3d' ),
		'id'            => 'sidebar-1',
		'description'   => esc_html__( 'Add widgets here.', 'nq3d' ),
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );
}
add_action( 'widgets_init', 'nq3d_widgets_init' );



function nq3d_scripts() {

    // CSS

    wp_enqueue_style('nq3d-style', get_stylesheet_uri(), array(), null);
    wp_enqueue_style('nq3d-style-fancybox', get_template_directory_uri().'/css/jquery.fancybox.min.css', array(), null);


    // JS
    wp_enqueue_script('nq3d-slick', get_template_directory_uri() . '/js/slick.min.js', array('jquery'), null, true);
    wp_enqueue_script('nq3d-selectBox', get_template_directory_uri() . '/js/jquery.selectBox.js', array('jquery'), null, true);
    wp_enqueue_script('nq3d-fancybox', get_template_directory_uri() . '/js/jquery.fancybox.min.js', array('jquery'), null, true);
    wp_enqueue_script('nq3d-navigation', get_template_directory_uri() . '/js/integration.js', array('jquery'), null, true);

    if (is_post_type_archive('programmes')) {

        wp_enqueue_script('nq3d-history', get_template_directory_uri() . '/js/history.js', array(), null, true );
        wp_enqueue_script('nq3d-filters', get_template_directory_uri() . '/js/filters.js', array('jquery'), null, true);

    }
}
add_action( 'wp_enqueue_scripts', 'nq3d_scripts' );



/**
 * WYSIWYG ACF - STYLES
 */

add_action('acf/input/admin_head', 'lh_acf_admin_head');
function lh_acf_admin_head() {
    ?>
    <style type="text/css">

        tr.acf-row:nth-child(even) > td {
            background: #f8f8f8 !important;
        }

        tr.acf-row:nth-child(odd) > td {
            background: #fefefe !important;
        }
    </style>
    <?php
}


/**
 * CF7 - remove p tags
 */
add_filter('wpcf7_autop_or_not', '__return_false');



require get_template_directory() . '/inc/template-tags.php';

require get_template_directory() . '/inc/template-functions.php';


// ============ NQ TOOLS ============

require get_template_directory() . '/inc/nq_custom.php';

require get_template_directory() . '/inc/nq_users.php';

require get_template_directory() . '/inc/nq_admin.php';

require get_template_directory() . '/inc/nq_front.php';


require get_template_directory() . '/inc/rgpd/rgpd.php';