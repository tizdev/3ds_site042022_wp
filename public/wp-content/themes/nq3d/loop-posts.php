<?php
global $iteration_posts;

$post_id = get_the_ID();
$post_title = get_the_title();
$post_link = get_permalink();
$post_date = get_the_date('d F Y');
$post_content = get_the_content();
$thumb_id = get_post_thumbnail_id();


if ( has_post_thumbnail($post_id) ) {
    $post_image = wp_get_attachment_image_src($thumb_id,'image_600x300', true);
    $post_image_url = $post_image[0];
} else {
    $post_image_url = get_template_directory_uri().'/img/default/image_600x300.jpg';
}

$html_post = '';

$html_post .= '<div class="post-item">';
    $html_post .= '<a class="post-item-image" href="' .$post_link. '" title="' .esc_attr($post_title). '">';
        $html_post .= '<img src="' .$post_image_url. '" alt="' .esc_attr($post_title). '"/>';
        /*$html_post .= '<img src="' .$post_image_url. '" srcset="' .$post_image_url. ' 430w, '.$post_image_url.' 620w" alt="' .esc_attr($post_title). '"/>';*/
    $html_post .= '</a>';
    $html_post .= '<div class="post-item-content">';
        $html_post .= '<div class="post-item-date">' .$post_date. '</div>';
        $html_post .= '<h3 class="post-item-title"><a href="'.$post_link.'" class="post-item-title ">' .$post_title. '</a></h3>';
        $html_post .= !empty($post_content) ? '<div class="post-item-excerpt">' .wp_html_excerpt($post_content, 200,'...' ). '</div>' : '';
        $html_post .= '<a class="btn btn-black" href="' .$post_link. '" title="' .__("Lire l'article", "nq3d"). '">' .__("Lire l'article","nq3d"). '</a>';
    $html_post .= '</div>';
$html_post .= '</div>';

echo $html_post;