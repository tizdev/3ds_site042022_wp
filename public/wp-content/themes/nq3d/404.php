<?php
/**
 * The template for displaying 404 pages (not found)
 *
 * @link https://codex.wordpress.org/Creating_an_Error_404_Page
 *
 * @package NQ_3D_Sélection
 */

get_header();

?>

<section class="error-404 not-found">

    <header class="page-header">
        <div class="not-found-banner">
            <img src="<?php echo  get_template_directory_uri() . '/img/img-404.jpg' ?>" alt="">
        </div>
        <div class="not-found-content">
            <h1 class="page-title not-found-title"><?php esc_html_e( 'Erreur 404', 'nq3d' ); ?></h1>

            <p><?php esc_html_e( 'Nous sommes désolés, la page demandée est inexistante.', 'nq3d' ); ?></p>
        </div>
    </header>

    <div class="page-content">
        <div class="page-action not-found-action">
            <?php echo '<a class="btn btn-black" href="' .get_permalink(ID_PAGE_ACCUEIL). '">' .__('Retour à l\'Accueil', 'nq3d'). '</a>'; ?>
        </div>
    </div>

</section>

<?php
get_footer();