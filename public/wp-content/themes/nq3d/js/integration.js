( function($) {

    // ============================================================================================================ //
    //                                                GLOBAL VARS
    // ============================================================================================================ //
    var body = $('body');
    var header = $('.site-header');
    var headerHeight = $('.site-header:not(.is-sticky)').outerHeight();


    // ============================================================================================================ //
    //                                              FUNCTIONS CALL
    // ============================================================================================================ //

    // OPEN & CLOSE MODALS
    // ====================================
    defineModalElement('.connection', '.js-btn-connection');
    defineModalElement('.inscription', '.js-btn-inscription');

    // CONNECTION ON LOAD
    // ====================================
    // if ($('.connection-onload').length) {
        // window.setTimeout(function() {
        //     body.toggleClass('connection-is-open');
        // }, 500);
    // }

    // DOT SLIDE
    // ====================================
    $('.slider-nav-dots').each(function() {
        var context = $(this).closest('section'),
            slides = $(context).find('.js-single-slide'),
            dots = $(this).children('.circle-inner');

        if ($(slides).length && $(dots).length) {
            $(dots).each(function() {
                var index = $(this).data('index');
                $(this).click(function() { slideEvent(this, index) });
            });
        }
    });

    // ARROW SLIDE
    // ====================================
    $('.slider-nav-arrows').each(function() {
        var context = $(this).closest('section'),
            slides = $(context).find('.js-single-slide'),
            arrows = $(this).children('.arrow');

        if ($(slides).length && $(arrows).length) {

            $(arrows).each(function() {

                $(this).click(function() {
                    var dir = $(this).data('dir'),
                        index,
                        dot,
                        currentSlide = $(context).find('.js-single-slide.is-visible');

                    if (dir === 'next') {
                        index = ($(currentSlide).next().hasClass('js-single-slide')
                                ? $(currentSlide).next().data('slide')
                                : 1 );
                        dot = $(context).find('.circle-inner[data-index=' + index + ']');
                    } else if (dir === 'prev') {
                        index = ($(currentSlide).prev().hasClass('js-single-slide')
                                ? $(currentSlide).prev().data('slide')
                                : $(slides).length );
                        dot = $(context).find('.circle-inner[data-index=' + index + ']');
                    }

                    slideEvent(dot, index)
                });
            });
        }
    });


    // SLICK SLIDES
    // ====================================
    initSlick('.js-programs-slick', 3, true);
    initSlick('.js-sp-slick', 1, true);


    // ANCHORS
    // ====================================
    $('a[href^="#"]').click(function() {
        var the_id = $(this).attr('href'),
            headerStickyHeight = $('.site-header.is-sticky').outerHeight();
        if (the_id === '#') {
            return;
        }
        $('html, body').animate({
            scrollTop: ($(the_id).offset().top - headerStickyHeight - 30)
        }, 'slow');
        console.log(headerStickyHeight);
        return false;
    });


    // MOBILE MENU
    // ====================================
    $('.js-open-menu, .js-close-menu').click(function() {
      $(body).toggleClass('menu-is-open');
    });

    // ============================================================================================================ //
    //                                                   EVENTS
    // ============================================================================================================ //
    $(window).scroll(function() {
        sticky();
        setHeaderHeight();
    });

    $(window).resize(function() {
        var size = $(window).width();

        sticky();
        setHeaderHeight();
        stopTransitions();

        if (size >= 540) {
          setSameHeight('.card-content');
        }
        // setSameHeight('.h-about-content');
        // setSameHeight('.h-preview-content');

        if (body.hasClass('menu-is-open') && size >= 767) {
          $('.js-close-menu').trigger('click');
        }
    });

    $(document).mouseup(function(e) {
        isModalOpen(e);
    });

    // HOME - MOB // LIRE LA SUITE
    // ====================================
    $('.h-about-content').on('click', function () {
        $('.h-about-content').toggleClass('open-content');

        if( $('.h-about-content').hasClass('open-content')) {
            $('.h-about-content p').slideDown();
        } else {
            $('.h-about-content p:nth-child(2)').slideUp();
        }
    });


    // ============================================================================================================ //
    //                                                  FUNCTIONS
    // ============================================================================================================ //
    function getScrollBarWidth () {
        var $outer = $('<div>').css({visibility: 'hidden', width: 100, overflow: 'scroll'}).appendTo(body),
            widthWithScroll = $('<div>').css({width: '100%'}).appendTo($outer).outerWidth();
        $outer.remove();
        return 100 - widthWithScroll;
    }

    function setSameHeight(selectors) {
        var store = [];
        $(selectors).css('minHeight', '');

        $(selectors).each(function() {
            store.push($(this).outerHeight());
        }).css('minHeight', (Math.max.apply(Math, store) + 1));
    }

    function defineModalElement(modal, button) {
        $(button).click(function() {
            if ( $(modal).length ) {
                toggleModal(modal.substring(1, modal.length));
            }
        });
    }

    function toggleModal(modalClass) {
        body.toggleClass(modalClass + '-is-open').toggleClass('modal-is-open');

        if (body.hasClass('modal-is-open')) {
            body.css('marginRight', getScrollBarWidth());
            header.css('paddingRight', getScrollBarWidth());
        } else {
            body.css('marginRight', '');
            header.css('paddingRight', '');
        }
    }

    function isModalOpen(e) {
        if ( body.hasClass('modal-is-open')) {
            var currentModal = $('.modal:visible'),
                singleClass = $(currentModal).attr('class').split(' ')[1];

            if ( (!currentModal.is(e.target) && currentModal.has(e.target).length === 0) || ($(e.target).closest('.modal-close').length) ) {
                toggleModal(singleClass);
            }
        }
    }

    function initSlick(slider, slidesNbr, arrows) {
        if ($(slider).length) {

            if(slider == '.js-programs-slick') {
                $(slider).slick({
                    infinite: true,
                    cssEase: 'ease-out',
                    slidesToShow: slidesNbr,
                    slidesToScroll: slidesNbr,
                    arrows: false,
                    dots: false,
                    responsive: [
                        {
                            breakpoint: 760,
                            settings: {
                                centerMode: true,
                                centerPadding: '60px',
                                slidesToShow: 2
                            }
                        },
                        {
                            breakpoint: 640,
                            settings: {
                                centerMode: true,
                                centerPadding: '60px',
                                slidesToShow: 1
                            }
                        }
                    ]
                });
            } else if(slider == '.js-sp-slick') {
                $(slider).slick({
                    infinite: true,
                    cssEase: 'ease-out',
                    slidesToShow: slidesNbr,
                    slidesToScroll: slidesNbr,
                    arrows: false,
                    dots: false,
                    responsive: [
                        {
                            breakpoint: 1025,
                            settings: {
                                dots: true
                            }
                        },
                        {
                            breakpoint: 641,
                            settings: {
                                dots: false
                            }
                        }
                    ]
                });
            }

            if ($(arrows) !== undefined) {

                $(slider+'-prev').click(function () {
                    $(slider).slick('slickPrev');
                });

                $(slider+'-next').click(function () {
                    $(slider).slick('slickNext');
                });
            }
        }
    }

    function setHeaderHeight() {
        headerHeight = $('.site-header:not(.is-sticky)').outerHeight();
    }

    function sticky() {
        var scroll = $(window).scrollTop();
        var windowSize = $(window).width();

        if (scroll >= 200 && windowSize >= 767) {
            if (!header.hasClass('is-sticky')) {
                body.css('paddingTop', headerHeight);
                header.addClass('is-sticky');
            }
        } else {
            if (header.hasClass('is-sticky')) {
                body.css('paddingTop', '');
                header.removeClass('is-sticky');
            }
        }
    }

    function slideEvent(dot, index) {
        var context = $(dot).closest('section');

        if (index) {
            var target = $(context).find('.js-single-slide[data-slide=' + index + ']');

            if (target.length) {
                $(dot).addClass('is-active').siblings().removeClass('is-active');
                $(target).addClass('is-visible').siblings().removeClass('is-visible');
            }
        }
    }

    function stopTransitions() {
      var elements = ['.header-nav, .header-infos-contact'];

      jQuery.each(elements, function(index, item) {
        $(item).css('transition', 'none');
      });

      setTimeout(function(){
        jQuery.each(elements, function(index, item) {
          $(item).css('transition', '');
        });
      }, 100);
    }


} )(jQuery);
