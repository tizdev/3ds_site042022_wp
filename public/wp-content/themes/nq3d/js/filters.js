(function ($) {

    var helpers = {
        debounce: function (func, wait, scope) {
            var timeout;
            return function () {
                var context = scope || this, args = arguments;
                var later = function () {
                    timeout = null;
                    func.apply(context, args);
                };
                clearTimeout(timeout);
                timeout = setTimeout(later, wait);
            };
        },

        throttle: function (fn, threshhold, scope) {
            threshhold || (threshhold = 250);
            var last,
                deferTimer;
            return function () {
                var context = scope || this;

                var now = +new Date,
                    args = arguments;
                if (last && now < last + threshhold) {
                    // hold on to it
                    clearTimeout(deferTimer);
                    deferTimer = setTimeout(function () {
                        last = now;
                        fn.apply(context, args);
                    }, threshhold);
                } else {
                    last = now;
                    fn.apply(context, args);
                }
            };
        }
    };



    $(window).on('resize orientationchange', helpers.debounce(function() {
        if($('.nq3d-filters-select-div').length) {
            if(!isMobileWidth() && $('.nq3d-filters-select-div').is(':hidden')) {
                $('.nq3d-filters-select-div').show();
            }
        }
    }, 100));

    // =======================================
    // GLOBAL > RESPONSIVE MARKERS
    // =======================================
    function isTabletLandscapeWidth() {

        var $landscapeRspMarker = $('.rwd-marker-1024');
        return $landscapeRspMarker.css('display') === 'none';

    }

    function isTabletPortraitWidth() {

        var $portraitRspMarker = $('.rwd-marker-768');
        return $portraitRspMarker.css('display') === 'none';

    }

    function isMobileWidth() {

        var $mobileRspMarker = $('.rwd-marker-640');
        return $mobileRspMarker.css('display') === 'none';

    }

    // SELECT CUSTOM
    // ====================================
    function select() {
        if (!isTabletLandscapeWidth() && !isTabletPortraitWidth() && !isMobileWidth()) {
            $('.nq3d-filters-select').selectBox({
                mobile: true,
                loopOptions: true,
                keepInViewport: false,
                menuTransition: 'slide',
                menuSpeed: 'fast',
                hideOnWindowScroll: false
            });
        } else if(isMobileWidth()) {
            $('.nq3d-filters-select').selectBox({
                mobile: false,
                loopOptions: true,
                keepInViewport: false,
                menuTransition: 'slide',
                menuSpeed: 'fast',
                hideOnWindowScroll: false
            });
        } else if(isTabletPortraitWidth() || isTabletLandscapeWidth()) {
            $('.nq3d-filters-select').selectBox({
                mobile: true,
                loopOptions: true,
                keepInViewport: false,
                menuTransition: 'slide',
                menuSpeed: 'fast',
                hideOnWindowScroll: false
            });
        }
    }

    $(document).ready(function () {
        select();

        initFiltersSelect();

        initFiltersEvents();

        initFiltersReset();

        mobFilter();

    });


    function mobFilter() {
        $('.nq3d-filters-title').on('click', function () {
            $('.nq3d-filters-mob').toggleClass('open');

            if ($('.nq3d-filters-mob').hasClass('open')) {
                $('.nq3d-filters-select-div').slideDown();
            } else {
                $('.nq3d-filters-select-div').slideUp();
            }
        });
    }


    function initFiltersEvents() {

        initDeleteFiltersEvent();

        initPaginationClickEvent();

        initSubmitFormEvent();

        select();

    }


    function initSubmitFormEvent() {

        $('.nq3d-filters-form').off('submit');
        $('.nq3d-filters-form').on('submit', function () {

            buildUrlWithParams();

            return false;

        });

    }


    function initPaginationClickEvent() {

        $('a.page-numbers').off('click');
        $('a.page-numbers').on('click', function () {

            var targetPaginationURL = $(this).attr('href');

            refreshFiltersURL(targetPaginationURL);

            return false;

        });

    }

    function initDeleteFiltersEvent() {

        $('.nq3d-filters-delete').off('click');
        $('.nq3d-filters-delete').on('click', function () {

            var $that = $(this);
            var $thatParentLi = $that.parent();
            var thatItemTaxSlug = $thatParentLi.attr('data-selected-item');
            var newVal = (thatItemTaxSlug == 'promoteur') ? 'tous' : 'toutes';

            $('select.nq3d-filters-select[data-url-slug="' + thatItemTaxSlug + '"]').val(newVal);

            $thatParentLi.fadeOut();

            buildUrlWithParams();

        });

    }


    function refreshFiltersURL(refreshUrl) {

        if (refreshUrl) {

            if (supportHistory()) {

                window.history.pushState(null, document.title, refreshUrl);
                initAjaxRefresh(refreshUrl);

            } else {

                window.location.href = refreshUrl;
            }

        }

    }


    function initAjaxRefresh(url) {

        if (url) {

            $('.nq3d-filters-ajax').fadeTo(0.2);

            $.get(
                url,
                function (html) {

                    var dom = $(html),
                        posts = dom.find('.nq3d-filters-ajax');

                    window.setTimeout(function () {

                        $('.nq3d-filters-ajax').replaceWith(posts).fadeTo(1);

                        // var listingTop = 0;
                        // if($('.nq3d-filters-ajax').length) {
                        //     listingTop = $('.nq3d-filters-ajax').offset().top;
                        // }
                        // $('html, body').animate({
                        //     scrollTop: listingTop - 50
                        // }, 1000);

                        initFiltersEvents();

                        mobFilter();

                        $('html, body').animate({
                            scrollTop: 0
                        }, 500);

                    }, 1000);

                }
            );

        }

    }


// ===============================================
// URLS PARAMS
// ===============================================

    function buildUrlWithParams() {

        var baseUrl = $('#filters-param-baseurl').val();
        var updatedURL = baseUrl;
        var isValueAll = true;


        $('select.nq3d-filters-select').each(function (index, elm) {

            var $that = $(this);

            var thatSlug = $that.attr('data-url-slug');
            var thatVal = $that.val();

            updatedURL += thatSlug + '/' + thatVal + '/';

            if (thatVal != 'tous' && thatVal != 'toutes') {
                isValueAll = false;
            }

        });

        if (isValueAll) {
            updatedURL = baseUrl;
        }


        refreshFiltersURL(updatedURL);

    }


// ===============================================
// RESET
// ===============================================

    function initFiltersReset() {


        $(document).on('click', '.nq3d-filters-reset', function () {

            $('select.nq3d-filters-select').each(function () {
                var $that = $(this);
                var defaultValue = $that.find('option').eq(0).attr('value');
                $that.val(defaultValue);
            });

            buildUrlWithParams();

            return false;

        });


    }

// ===============================================
// SELECT
// ===============================================

    function initFiltersSelect() {

        $(document).on('change', '.nq3d-filters-select', function () {

            buildUrlWithParams();

        });

    }


// ===============================================
// URL CHANGE
// ===============================================

    $(window).bind('popstate', function () {

        var State = History.getState(),
            currentURL = State.url;

        initAjaxRefresh(currentURL);

    });


    function supportHistory() {

        var ua = navigator.userAgent;
        if (
            (
                ua.indexOf('Android 2.') !== -1 ||
                ua.indexOf('Android 4.0') !== -1
            ) &&
            ua.indexOf('Mobile Safari') !== -1 &&
            ua.indexOf('Chrome') === -1 &&
            ua.indexOf('Windows Phone') === -1 &&
            location.protocol !== 'file:'
        ) {
            return false;
        }

        return window.history && 'pushState' in window.history;
    }


})
(jQuery);
