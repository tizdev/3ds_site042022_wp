<?php
if(!is_user_logged_in()){
    wp_redirect(home_url('/'));
    exit;
}

get_header();
the_post();

$programme_id = get_the_ID();
$programme_title = get_the_title();
$programme_link = get_permalink();
$programme_description = get_field('description_courte', $programme_id);
$programme_content = get_the_content();

$programme_galerie = get_field('galerie_images', $programme_id);
$programme_liste = get_field('liste_descriptive', $programme_id);
$programme_localisation = get_field('localisation', $programme_id);
$programme_code_postal = get_field('code_postal', $programme_id);
$programme_date_de_livraison = get_field('date_de_livraison', $programme_id);
$programme_coordonnees_gps = get_field('coordonnees_gps', $programme_id);
$programme_image_carte = get_field('image_carte', $programme_id);
$programme_url_video_youtube = get_field('url_video_youtube', $programme_id);
$programme_image_de_fond_video = get_field('image_de_fond_video', $programme_id);

$programme_localisation_tax = nq_get_tax_infos($programme_id,'tax-localisations');
$programme_localisation_html = '';
if( !empty($programme_localisation_tax['name']) || !empty($programme_localisation_tax['zipcode']) ){
    $programme_localisation_html .= '<span class="sp-header-location">';
    $programme_localisation_html .= !empty($programme_localisation_tax['name']) ? $programme_localisation_tax['name'] : '';
    $programme_localisation_html .= !empty($programme_localisation_tax['zipcode']) ? ' ('.$programme_localisation_tax['zipcode'].')' : '';
    $programme_localisation_html .= '</span>';
}

$programme_date_tax = nq_get_tax_infos($programme_id,'tax-dates');
$programme_date_html = '';
if( !empty($programme_date_tax['name'])){
    $programme_date_html .= '<span class="sp-header-tax">';
    $programme_date_html .= '<span class="sp-header-tax-title">'.__('Date d\'actabilité','nq3d').'</span>';
    $programme_date_html .= '<strong>' .$programme_date_tax['name']. '</strong>';
    $programme_date_html .= '</span>';
}

$programme_loi_tax = nq_get_tax_infos($programme_id,'tax-lois');
$programme_loi_html = '';
if( !empty($programme_loi_tax['name'])){
    $programme_loi_html .= '<span class="sp-header-tax">';
    $programme_loi_html .= '<span class="sp-header-tax-title">'.__('Loi','nq3d').'</span>';
    $programme_loi_html .= '<strong>' .$programme_loi_tax['name']. '</strong>';
    $programme_loi_html .= '</span>';
}

$programme_promoteur_tax = nq_get_tax_infos($programme_id,'tax-promoteurs');

$programme_doc_1 = get_field('maquette_commerciale', $programme_id);
$programme_doc_2 = get_field('contrat_de_reservation', $programme_id);
$programme_docs = get_field('autres_documents', $programme_id);

// BREADCRUMB
// =============================

echo '<div class="breadcrumb container" style="height:62px">' .nq_inlinesvg('chevron'). '<a href="' .get_post_type_archive_link('programmes'). '" title="' .__('Liste des programmes', 'nq3d'). '">' .__('Liste des programmes', 'nq3d'). '</a></div>';


if( nq_users_is_pending() ) :

    echo '<div class="nq-notice-pending">';
        echo '<p>'.__('Votre compte est en attente de validation. Vous allez recevoir un email lors de son activation','nq3d').'</p>';
    echo '</div>';

else :


    echo '<article class="sp-article">';


        // HEADER PART
        // =============================
        ?>

        <div class="sp-header<?php echo (empty($programme_galerie) ? ' sp-header-fullwidth' : ''); ?>">

            <?php

            $html_header = '';

            $html_header .= '<div class="container">';

            $html_header .= '<div class="sp-header-content">';
            $html_header .= '<h1 class="sp-header-title">' .$programme_title. '</h1>';
            $html_header .= $programme_localisation_html;
            $html_header .= !empty($programme_description) ? '<div class="sp-header-description">' .$programme_description. '</div>' : '';

            if ( !empty($programme_loi_html) || !empty($programme_date_html) ) {
                $html_header .= '<div class="sp-header-metas">';
                $html_header .= $programme_loi_html;
                $html_header .= (!empty($programme_date_html) && !empty($programme_loi_html)) ? '<div class="sp-header-divider"></div>' : '';
                $html_header .= $programme_date_html;
                $html_header .= '</div>';
            }
            $html_header .= '</div>';


            if ( !empty($programme_galerie) ) {

                $html_header .= '<div class="sp-header-gallery">';
                $html_header .= '<div class="sp-header-gallery-slider' .(count($programme_galerie) >= 2 ? ' js-sp-slick' : ''). '">';

                foreach($programme_galerie as $galerie_image){
                    $html_header .= '<div class="sp-header-gallery-slide">';
                    $html_header .= '<img src="' .$galerie_image['sizes']['image_780x385']. '" alt="" />';
                    $html_header .= '</div>';
                }

                $html_header .= '</div>';
                $html_header .= '</div>';

            }

            if ( count($programme_galerie) >= 2) {
                $html_header .= '<div class="slider-nav-arrows">';
                $html_header .= '<span class="arrow js-sp-slick-prev" data-dir="prev">';
                $html_header .= nq_inlinesvg('prev');
                $html_header .= '</span>';
                $html_header .= '<span class="arrow js-sp-slick-next" data-dir="next">';
                $html_header .= nq_inlinesvg('next');
                $html_header .= '</span>';
                $html_header .= '</div>';
            }

            $html_header .= '</div>';

            echo $html_header;

            ?>

        </div>

        <?php



        // CONTENT PART
        // =============================

        if ( !empty($programme_content) || !empty($programme_liste) ) :

            $fullWidth = (empty($programme_liste) || empty($programme_content)) ? ' sp-content-fullwidth' : '';
            $oneSlide =  empty(count($programme_galerie) >= 2) ? ' sp-content-oneSlide' : '';

            ?>

            <div class="sp-content<?php echo $fullWidth; echo $oneSlide; ?>">
                <div class="container">

                    <?php

                    $html_content = '';

                    $html_content .= '<h4 class="sp-content-title">' .__('Description','nq3d'). '</h4>';

                    if ( !empty($programme_content) ) {
                        $html_content .= '<div class="sp-content-description">';
                        $html_content .= apply_filters('the_content', $programme_content );
                        $html_content .= '</div>';
                    }

                    if ( !empty($programme_liste) ) {
                        $programme_listes = array_chunk(
                            $programme_liste,
                            ceil(count($programme_liste)/2)
                        );

                        $html_content .= '<div class="sp-content-list">';

                        foreach($programme_listes as $programme_ul) {
                            $html_content .= '<ul class="sp-content-single-list">';
                            foreach ($programme_ul as $programme_li) {
                                $html_content .= '<li>' . $programme_li['description'] . '</li>';
                            }
                            $html_content .= '</ul>';
                        }

                        $html_content .= '</div>';
                    }

                    echo $html_content;

                    ?>

                </div>
            </div>

        <?php endif;



        // PROMOTEUR / LOCALISATION PART
        // =============================

        if ( !empty($programme_localisation) || !empty($programme_code_postal) || !empty($programme_date_de_livraison) || !empty($programme_coordonnees_gps) || !empty($programme_image_carte) || !empty($programme_promoteur_tax['name']) ) :

            ?>

            <div class="sp-location<?php echo (empty($programme_image_carte) ? ' sp-location-fullwidth' : ''); ?>">

                <?php

                $html_location = '';

                if ( !empty($programme_image_carte) ) {

                    $html_location .= '<div class="sp-location-image">';
                    $html_location .= '<img src="' .$programme_image_carte['sizes']['image_330x270']. '" alt=""/>';
                    $html_location .= '</div>';
                }

                $html_location .= '<div class="sp-location-content">';

                if ( !empty($programme_promoteur_tax['logo']) ) {
                    $html_location .= '<img src="' .$programme_promoteur_tax['logo']['sizes']['image_200xauto']. '" alt=""/>';
                }

                if ( !empty($programme_promoteur_tax['slug']) && !empty($programme_promoteur_tax['name']) ) {
                    $html_location .= '<div class="sp-sponsor-link"><a href="' .get_post_type_archive_link('programmes').'promoteur/'.$programme_promoteur_tax['slug'].'/date-actabilite/toutes/localisation/toutes/loi/toutes/budget/tous" title="">' .__('Voir tous les projets','nq3d'). ' ' .$programme_promoteur_tax['name']. '</a></div>';
                }

                $html_location .= !empty($programme_localisation) ? '<p><strong>' .__('Localisation','nq3d'). ' : </strong>' .$programme_localisation.'</p>' : '';
                $html_location .= '<p>';
                $html_location .= !empty($programme_code_postal) ? '<strong>' .__('Code postal','nq3d'). ' : </strong>' .$programme_code_postal.'<br/>' : '';
                $html_location .= !empty($programme_date_de_livraison) ? '<strong>' .__('Livraison','nq3d'). ' : </strong>' .$programme_date_de_livraison.'' : '';
                $html_location .= '</p>';
                $html_location .= !empty($programme_coordonnees_gps) ? '<p><strong>' .__('GPS','nq3d'). ' : </strong>' .$programme_coordonnees_gps. '</p>' : '';

                $html_location .= '</div>';

                echo $html_location;

                ?>

           </div>

        <?php endif;



        // VIDEO PART
        // =============================

        if ( !empty($programme_url_video_youtube) && (preg_match('%(?:youtube(?:-nocookie)?\.com/(?:[^/]+/.+/|(?:v|e(?:mbed)?)/|.*[?&]v=)|youtu\.be/)([^"&?/ ]{11})%i', $programme_url_video_youtube, $match)) ) :

            $video_slug = $match[1];

            $default_bkg_image = 'https://img.newquest.fr/1400/500/BBB/'; // fallback
            if ( !empty($programme_image_de_fond_video['sizes']['image_1400x500']) ) {
                $default_bkg_image = $programme_image_de_fond_video['sizes']['image_1400x500'];
            }

            ?>

            <div class="sp-video" style="background: url('<?php echo $default_bkg_image; ?>') center center / cover no-repeat">
                <div class="container">

                    <?php

                    $html_video = '';

                    // TODO AJOUTER JS POUR LIGHTBOX VIDEO
                    // https://www.youtube.com/watch?v='.$video_slug.'&autoplay=1&rel=0&controls=0&showinfo=0

                    $html_video .= '<div class="sp-video-content" data-video="' .$video_slug. '">';
                    $html_video .= '<span class="sp-video-inner">';
                    $html_video .= '<a class="sp-video-link" data-fancybox="" data-options="{&quot;iframe&quot; : {&quot;css&quot; : {&quot;width&quot; : &quot;80%&quot;, &quot;height&quot; : &quot;80%&quot;}}}" href="https://www.youtube.com/watch?v='.$video_slug.'&autoplay=1&rel=0&controls=0&showinfo=0" class="btn btn-primary">';
                    $html_video .= '<h1 class="sp-video-title">' .__('Vidéo','nq3d'). '</h1>';
                    $html_video .= '<span class="sp-video-subtitle">' .__('Découvrez le projet de A à Z','nq3d'). '</span>';
                    $html_video .= '<span class="sp-video-icon">' .nq_inlinesvg('play'). '</span>';
                    $html_video .= '</a>';
                    $html_video .= '</span>';
                    $html_video .= '</div>';

                    echo $html_video;

                    ?>

                </div>
            </div>

        <?php endif;



        // DOCS PART
        // =============================

        $programme_doc_1 = get_field('maquette_commerciale', $programme_id);
        $programme_doc_2 = get_field('contrat_de_reservation', $programme_id);
        $programme_docs = get_field('autres_documents', $programme_id);

        if ( !empty($programme_doc_1) || !empty($programme_doc_1) || !empty($programme_docs) ) :

            echo '<div class="sp-docs">';
                echo '<div class="container">';

                    echo '<h2 class="sp-docs-title">' .__('Documents utiles','nq3d'). '</h2>';

                    echo '<div class="sp-docs-header">';
                        echo !empty($programme_doc_1['url']) ? '<a href="'.$programme_doc_1['url'].'" download class="sp-docs-single">' .nq_inlinesvg('pdf'). '<span>' .__('Maquette commerciale','nq3d'). '</span></a>' : '';
                        echo !empty($programme_doc_2['url']) ? '<a href="'.$programme_doc_2['url'].'" download class="sp-docs-single">' .nq_inlinesvg('pdf'). '<span>' .__('Contrat de réservation','nq3d'). '</span></a>' : '';
                    echo '</div>';

                    if ( have_rows('autres_documents') ) :
                        echo '<div class="sp-docs-list">';
                            while( have_rows('autres_documents') ) : the_row();
                                $doc = get_sub_field('document');
                                echo !empty($doc['url']) ? '<a href="' .$doc['url']. '" download class="sp-docs-single">' .nq_inlinesvg('pdf'). '<span>' .$doc['title']. '</span></a>' : '';
                            endwhile;
                        echo '</div>';
                    endif;

                echo '</div>';
            echo '</div>';

        endif;


    echo '</article>';

endif;

get_footer();