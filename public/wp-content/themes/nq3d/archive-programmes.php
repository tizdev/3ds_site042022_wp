<?php
if (!is_user_logged_in()) {
    wp_redirect(home_url('/'));
    exit;
}

get_header();
?>


    <header class="page-header">
        <div class="container">
            <?php
            echo '<div class="breadcrumb">' . nq_inlinesvg('chevron') . '<a href="' . home_url() . '" title="' . __('Accueil', 'nq3d') . '">' . __('Accueil', 'nq3d') . '</a></div>';
            echo '<h1>' . __('Liste des programmes', 'nq3d') . '</h1>';
            ?>
        </div>
    </header>


<?php

if (nq_users_is_pending()) :

    echo '<div class="nq-notice-pending">';
    echo '<p>' . __('Votre compte est en attente de validation. Vous allez recevoir un email lors de son activation', 'nq3d') . '</p>';
    echo '</div>';

else :

    ?>

    <section class="nq3d-filters-ajax p-programs">
        <div class="container">

            <?php
            $html_selection_localisation = '';
            $html_selection_promoteur = '';
            $html_selection_loi = '';
            $html_selection_date = '';
            $html_selection_budget = '';
            $has_selection = false;

            if (get_query_var('promoteur') && get_query_var('promoteur') != 'toutes') {
                $promoteur_esc = esc_attr(get_query_var('promoteur'));
                $promoteur_exists = term_exists($promoteur_esc, 'tax-promoteurs');

                if ($promoteur_exists !== 0 && $promoteur_exists !== null) {
                    $promoteur_obj = get_term_by('slug', $promoteur_esc, 'tax-promoteurs');
                    $promoteur_name = $promoteur_obj->name;
                    $promoteur_slug = $promoteur_obj->slug;

                    $html_selection_promoteur .= '<li data-selected-item="promoteur" data-item-slug="' . $promoteur_slug . '">';
                    $html_selection_promoteur .= nq_inlinesvg('close', 'close nq3d-filters-delete') . '<span class="txt">' . $promoteur_name . '</span>';
                    $html_selection_promoteur .= '</li>';

                    $has_selection = true;
                }
            }

            if (get_query_var('date-actabilite') && get_query_var('date-actabilite') != 'toutes') {
                $date_esc = esc_attr(get_query_var('date-actabilite'));
                $date_exists = term_exists($date_esc, 'tax-dates');

                if ($date_exists !== 0 && $date_exists !== null) {
                    $date_obj = get_term_by('slug', $date_esc, 'tax-dates');
                    $date_name = $date_obj->name;
                    $date_slug = $date_obj->slug;

                    $html_selection_date .= '<li data-selected-item="date-actabilite" data-item-slug="' . $date_slug . '">';
                    $html_selection_date .= nq_inlinesvg('close', 'close nq3d-filters-delete') . '<span class="txt">' . $date_name . '</span>';
                    $html_selection_date .= '</li>';

                    $has_selection = true;
                }
            }

            if (get_query_var('localisation') && get_query_var('localisation') != 'toutes') {
                $localisation_esc = esc_attr(get_query_var('localisation'));
                $localisation_exists = term_exists($localisation_esc, 'tax-localisations');

                if ($localisation_exists !== 0 && $localisation_exists !== null) {
                    $localisation_obj = get_term_by('slug', $localisation_esc, 'tax-localisations');
                    $localisation_name = $localisation_obj->name;
                    $localisation_slug = $localisation_obj->slug;

                    $html_selection_localisation .= '<li data-selected-item="localisation" data-item-slug="' . $localisation_slug . '">';
                    $html_selection_localisation .= nq_inlinesvg('close', 'close nq3d-filters-delete') . '<span class="txt">' . $localisation_name . '</span>';
                    $html_selection_localisation .= '</li>';

                    $has_selection = true;
                }
            }

            if (get_query_var('loi') && get_query_var('loi') != 'toutes') {
                $loi_esc = esc_attr(get_query_var('loi'));
                $loi_exists = term_exists($loi_esc, 'tax-lois');

                if ($loi_exists !== 0 && $loi_exists !== null) {
                    $loi_obj = get_term_by('slug', $loi_esc, 'tax-lois');
                    $loi_name = $loi_obj->name;
                    $loi_slug = $loi_obj->slug;

                    $html_selection_loi .= '<li data-selected-item="loi" data-item-slug="' . $loi_slug . '">';
                    $html_selection_loi .= nq_inlinesvg('close', 'close nq3d-filters-delete') . '<span class="txt">' . $loi_name . '</span>';
                    $html_selection_loi .= '</li>';

                    $has_selection = true;
                }
            }

            if (get_query_var('budget') && get_query_var('budget') != 'toutes') {
                $budget_esc = esc_attr(get_query_var('budget'));
                $budget_exists = term_exists($budget_esc, 'tax-budgets');

                if ($budget_exists !== 0 && $budget_exists !== null) {
                    $budget_obj = get_term_by('slug', $budget_esc, 'tax-budgets');
                    $budget_name = $budget_obj->name;
                    $budget_slug = $budget_obj->slug;

                    $html_selection_budget .= '<li data-selected-item="budget" data-item-slug="' . $budget_slug . '">';
                    $html_selection_budget .= nq_inlinesvg('close', 'close nq3d-filters-delete') . '<span class="txt">' . $budget_name . '</span>';
                    $html_selection_budget .= '</li>';

                    $has_selection = true;
                }
            }

            $programmes_promoteurs = get_terms(
                array(
                    'taxonomy' => 'tax-promoteurs',
                    'hide_empty' => true
                )
            );

            $programmes_localisations = get_terms(
                array(
                    'taxonomy' => 'tax-localisations',
                    'hide_empty' => true
                )
            );

            $programmes_dates = get_terms(
                array(
                    'taxonomy' => 'tax-dates',
                    'hide_empty' => true,
                    'orderby' => 'term_ID',
                    'order' => 'ASC'
                )
            );

            $programmes_lois = get_terms(
                array(
                    'taxonomy' => 'tax-lois',
                    'hide_empty' => true
                )
            );

            $programmes_budgets = get_terms(
                array(
                    'taxonomy' => 'tax-budgets',
                    'hide_empty' => true
                )
            );

            echo '<div class="nq3d-filters">';

            echo '<div class="nq3d-filters-inner">';

            echo '<form class="nq3d-filters-form">';

            echo '<button class="nq3d-filters-reset' . (!$has_selection ? ' nq3d-filters-reset-hidden' : '') . '">' . __('Réinitialiser les filtres', 'nq3d') . '</button>';

            echo '<div class="nq3d-filters-mob">';

            echo '<p class="nq3d-filters-title">' . __('Filtrer par :', 'nq3d') . '</p>';


            echo '<input type="hidden" name="filters-param-baseurl" id="filters-param-baseurl" class="" value="' . get_post_type_archive_link('programmes') . '" />';

            echo '<div class="nq3d-filters-select-div">';

            echo '<div class="field-filter filter-promoteur">';
            echo '<select class="nq3d-filters-select" data-url-slug="promoteur">';
            echo '<option value="tous">' . __('Promoteur', 'nq3d') . '</option>';
            if (!empty($programmes_promoteurs) && !is_wp_error($programmes_promoteurs)) {
                foreach ($programmes_promoteurs as $promoteur) {
                    $is_selected = (get_query_var('promoteur') == $promoteur->slug) ? ' selected="selected"' : '';
                    echo '<option value="' . $promoteur->slug . '"' . $is_selected . '>' . $promoteur->name . '</option>';
                }
            }
            echo '</select>';
            echo '</div>';

            echo '<div class="field-filter filter-date">';
            echo '<select class="nq3d-filters-select" data-url-slug="date-actabilite">';
            echo '<option value="toutes">' . __('Date d\'actabilité', 'nq3d') . '</option>';
            if (!empty($programmes_dates) && !is_wp_error($programmes_dates)) {
                foreach ($programmes_dates as $date) {
                    $is_selected = (get_query_var('date-actabilite') == $date->slug) ? ' selected="selected"' : '';
                    echo '<option value="' . $date->slug . '"' . $is_selected . '>' . $date->name . '</option>';
                }
            }
            echo '</select>';
            echo '</div>';

            echo '<div class="field-filter filter-localisation">';
            echo '<select class="nq3d-filters-select" data-url-slug="localisation">';
            echo '<option value="toutes">' . __('Localisation', 'nq3d') . '</option>';
            if (!empty($programmes_localisations) && !is_wp_error($programmes_localisations)) {
                foreach ($programmes_localisations as $localisation) {
                    $is_selected = (get_query_var('localisation') == $localisation->slug) ? ' selected="selected"' : '';
                    echo '<option value="' . $localisation->slug . '"' . $is_selected . '>' . $localisation->name . '</option>';
                }
            }
            echo '</select>';
            echo '</div>';

            echo '<div class="field-filter filter-loi">';
            echo '<select class="nq3d-filters-select" data-url-slug="loi">';
            echo '<option value="toutes">' . __('Loi', 'nq3d') . '</option>';
            if (!empty($programmes_lois) && !is_wp_error($programmes_lois)) {
                foreach ($programmes_lois as $loi) {
                    $is_selected = (get_query_var('loi') == $loi->slug) ? ' selected="selected"' : '';
                    echo '<option value="' . $loi->slug . '"' . $is_selected . '>' . $loi->name . '</option>';
                }
            }
            echo '</select>';
            echo '</div>';

            echo '<div class="field-filter filter-budget">';
            echo '<select class="nq3d-filters-select" data-url-slug="budget">';
            echo '<option value="tous">' . __('Budget', 'nq3d') . '</option>';
            if (!empty($programmes_budgets) && !is_wp_error($programmes_budgets)) {
                foreach ($programmes_budgets as $budget) {
                    $is_selected = (get_query_var('budget') == $budget->slug) ? ' selected="selected"' : '';
                    echo '<option value="' . $budget->slug . '"' . $is_selected . '>' . $budget->name . '</option>';
                }
            }
            echo '</select>';
            echo '</div>';

            echo '</div>';


            echo '</div>';

            echo '</form>';

            echo '</div>';

            echo '</div>';



            if ($has_selection) {

                echo '<div class="nq3d-filters-active">';
                echo '<ul class="nq3d-filters-active-items">';
                echo $html_selection_promoteur;
                echo $html_selection_date;
                echo $html_selection_localisation;
                echo $html_selection_loi;
                echo $html_selection_budget;
                echo '</ul>';
                echo '</div>';

            }


            if (have_posts()) {

                echo '<div class="cards-row">';

                global $iteration_programmes;
                $iteration_programmes = 0;

                while (have_posts()) {
                    the_post();

                    get_template_part('loop', 'programmes');

                    $iteration_programmes++;

                }

                echo '</div>';

                if( function_exists('nq_pagination')){
                    nq_pagination();
                }


            } else {

                echo '<p class="noresult ff_ital">' . __('Aucun programme pour le moment.', 'nq3d') . '</p>';

            } ?>


        </div>
    </section>


<?php

endif;

get_footer();