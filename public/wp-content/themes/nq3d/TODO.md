
# TODO

## Global

* Déconnexion qui ramène toujours à la page d'accueil ?
* Fallback des images à changer (https://img.newquest.fr/...).
* Corriger message d'érreur quand on essaye de se connecter avec un profil qui n'est pas admin.


## Page Accueil

* __Section `h-preview` :__ faire l'animation du slider et ajouter dynamiquement la localisation `Pontcharra (à changer)`.
* __Section `h-banner` :__ enlever la redirection du bouton connection vers la page `Programmes`.
* __Section `h-about` :__ faire l'animation du slider.
* __Global:__ faire les déclinaisons responsive.


## Page Actualités

* Fixer le nombre de posts à 5 par page.
* Traitement des thumbnails : __srcset__ ?


## Page Programmes

* Corriger conflit JS quand on ajoute des filtres. Ça retire les `min-height` définis par `setSameHeight()` dans `integration.js` et donc casse la grille.
* Design + déclinaisons résponsives des filtres.


## Single Programme

* Modal pour afficher la vidéo.
* Attributs alt/title des images.
* Mettre les bons icons PDF.
* Faire les déclinaisons responsive.


## Page CMS

* À continuer.


## Page 404

* À faire
