<?php

$programme_id = get_the_ID();
$programme_title = get_the_title();
$programme_link = get_permalink();
$programme_description = get_field('description_courte', $programme_id);
if(empty($programme_description)){
    $programme_description = get_the_content();
}

$programme_localisation_tax = nq_get_tax_infos($programme_id,'tax-localisations');
$programme_localisation_html = '';
if( !empty($programme_localisation_tax['name']) || !empty($programme_localisation_tax['zipcode']) ){
    $programme_localisation_html .= '<span class="card-content-location">';
    $programme_localisation_html .= !empty($programme_localisation_tax['name']) ? $programme_localisation_tax['name'] : '';
    $programme_localisation_html .= !empty($programme_localisation_tax['zipcode']) ? ' ('.$programme_localisation_tax['zipcode'].')' : '';
    $programme_localisation_html .= '</span>';
}

if (has_post_thumbnail($programme_id)) {
    $programme_image = wp_get_attachment_image_src(get_post_thumbnail_id($programme_id), 'image_485x323');
    $programme_image_url = $programme_image[0];

} else {
    $programme_image_url = get_template_directory_uri() . '/img/default/image_485x323.jpg';
}

$html_programme = '';

$html_programme .= '<div class="card">';
$html_programme .= '<div class="card-header">';
$html_programme .= '<a href="' . $programme_link . '" title="' . esc_attr($programme_title) . '" class="card-header-link">';
$html_programme .= '<img src="' .$programme_image_url.'" alt="' . esc_attr($programme_title) . '" class="card-header-img" />';
$html_programme .= '</a>';
$html_programme .= '</div>';
$html_programme .= '<div class="card-content">';
$html_programme .= '<h3 class="card-content-title"><a href="' . $programme_link . '">' . $programme_title.'</a></h3>';
$html_programme .= $programme_localisation_html;
$html_programme .= !empty($programme_description) ? '<p class="card-content-excerpt">'.wp_html_excerpt($programme_description, 65, '...').'</p>' : '' . "\n";
$html_programme .= '</div>';
$html_programme .= '</div>';

echo $html_programme;
