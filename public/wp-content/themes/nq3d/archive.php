<?php
get_header();

$archive_title = '';

if ( is_category() ) {

	$archive_title = sprintf( __( '%s' ), single_cat_title( '', false ) );
	$archive_description = category_description();

} elseif ( is_tag() ) {

    $archive_title = sprintf( __( '%s' ), single_tag_title( '', false ) );

} elseif ( is_author() ) {

    $archive_title = sprintf( __( 'Auteur : %s' ), '<span class="vcard">' . get_the_author() . '</span>' );

} elseif ( is_year() ) {

    $archive_title = sprintf( __( 'Année %s' ), get_the_date( _x( 'Y', 'yearly archives date format' ) ) );

} elseif ( is_month() ) {

    $archive_title = sprintf( __( '%s' ), get_the_date( _x( 'F Y', 'monthly archives date format' ) ) );

} elseif ( is_day() ) {

    $archive_title = sprintf( __( '%s' ), get_the_date( _x( 'F j, Y', 'daily archives date format' ) ) );

} else {

    $archive_title = __( 'Archives', 'nq3d' );

}


echo '<hr />';

echo '<header class="pages-header">';
echo '<h1 class="pages-header-title">'.$archive_title.'</h1>';
echo '</header>';

echo '<hr />';



echo '<section class="posts-listing-wrapper clearfix">';

	if ( have_posts() ){

		echo '<div class="posts-listing clearfix">';

		global $iteration_posts;
		$iteration_posts = 0;

		while ( have_posts() ){
			the_post();

			get_template_part('loop', 'posts');

			$iteration_posts++;

		}

		echo '</div>';

		if( function_exists('nq_pagination')){
			nq_pagination();
		}

	}else{

		echo '<p class="noresult ff_ital">'.__('Aucune actualité pour le moment.','nq3d').'</p>';

	}

echo '</section>';

get_footer();